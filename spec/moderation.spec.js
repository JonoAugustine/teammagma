const { setup, teardown } = require("./helper");
const { mock } = require("./mock");

describe("Moderation", () => {
  let db;
  let moderation;

  afterEach(async () => await teardown());

  test("Non auth cannot read", async () => {
    db = await setup();
    moderation = db.collection("moderation");

    await expect(moderation.get()).toDeny();
  });

  test("Non auth cannot write", async () => {
    db = await setup();
    moderation = db.collection("moderation");

    await expect(moderation.add({})).toDeny();
  });

  test("Non admin auth cannot read", async () => {
    db = await setup(mock.auth());
    moderation = db.collection("moderation");
    await expect(moderation.get()).toDeny();
  });

  test("Non admin auth cannot write", async () => {
    db = await setup(mock.auth());
    moderation = db.collection("moderation");
    await expect(moderation.add({})).toDeny();
  });

  test("Admin auth can read", async () => {
    db = await setup(mock.auth(true));
    moderation = db.collection("moderation");
    await expect(moderation.get()).toAllow();
  });

  test("Admin auth can write", async () => {
    db = await setup(mock.auth(true));
    moderation = db.collection("moderation");
    await expect(moderation.add({})).toAllow();
  });
});
