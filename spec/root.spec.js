const { setup, teardown } = require("./helper");

describe("Root rules", () => {
  let db;
  let ref;

  beforeAll(async () => {
    db = await setup();
    ref = db.collection("fake-collection");
  });

  afterAll(async () => await teardown());

  test("Cannot access fake collection", async () => {
    await expect(ref.add({})).toDeny();
    await expect(ref.get()).toDeny();
  });
});
