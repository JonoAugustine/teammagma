const { setup, teardown } = require("./helper");
const { mock, nullify } = require("./mock");

describe("Unauth Listing rules", () => {
  let db;
  let listings;

  beforeEach(async () => {
    db = await setup();
    listings = db.collection("listings");
  });

  afterEach(async () => teardown());

  test("Allow read", async () => {
    await expect(listings.get()).toAllow();
  });

  test("Cannot add poke listing", async () => {
    await expect(listings.add(mock.listing.poke()));
  });

  test("Cannot add item listing", async () => {
    await expect(listings.add(mock.listing.item()));
  });
});

describe("Auth Listing create rules", () => {
  let db;
  let listings;

  beforeEach(async () => {
    db = await setup(mock.auth());
    listings = db.collection("listings");
  });

  afterEach(async () => teardown());

  test("Cannot add empty objcet", async () => {
    await expect(listings.add({})).toDeny();
  });

  test("Cannot add poke listing with nulls", async () => {
    await expect(listings.add(nullify(mock.listing.poke()))).toDeny();
  });

  test("Cannot add item listing with nulls", async () => {
    await expect(listings.add(nullify(mock.listing.item()))).toDeny();
  });

  test("Can add valid poke listing", async () => {
    await expect(listings.add(mock.listing.poke())).toAllow();
  });

  test("Can add valid item listing", async () => {
    await expect(listings.add(mock.listing.item())).toAllow();
  });
});
