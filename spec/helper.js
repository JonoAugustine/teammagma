const firebase = require("@firebase/testing");
const fs = require("fs");

module.exports.setup = async (auth, data) => {
  const projectId = `rules-spec-${Date.now()}`;
  const app = await firebase.initializeTestApp({
    projectId,
    auth
  });

  const db = app.firestore();

  await firebase.loadFirestoreRules({
    projectId,
    rules: fs.readFileSync("./spec/test.rules", "utf8")
  });

  if (data) {
    for (const k in data) {
      await db.doc(k).set(data[k]);
    }
  }

  await firebase.loadFirestoreRules({
    projectId,
    rules: fs.readFileSync("firestore.rules", "utf8")
  });

  return db;
};

module.exports.teardown = async () => {
  Promise.all(firebase.apps().map(app => app.delete()));
};

expect.extend({
  async toAllow(x) {
    let pass = false;
    let err;
    try {
      await firebase.assertSucceeds(x);
      pass = true;
    } catch (e) {
      err = e;
    }

    return {
      pass,
      message: () =>
        `Expected Firebase op to be allowed but was denied.\n${err}`
    };
  }
});

expect.extend({
  async toDeny(x) {
    let pass = false;
    try {
      await firebase.assertFails(x);
      pass = true;
    } catch (err) {}

    return {
      pass,
      message: () => "Expected Firebase op to be denied but was allowed."
    };
  }
});
