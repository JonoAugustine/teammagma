const { setup, teardown } = require("./helper");
const { mock, nullify } = require("./mock");

describe("Unath Offer rules", () => {
  let db;
  let offers;

  beforeEach(async () => {
    db = await setup();
    offers = db.collection("offers");
  });

  afterEach(async () => teardown());

  test("Cannot read", async () => {
    await expect(offers.get()).toDeny();
    await expect(offers.doc("_").get()).toDeny();
  });

  test("Cannot add offer", async () => {
    await expect(offers.doc("_").set(mock.offer())).toDeny();
  });
});

describe("Auth Offer rules", () => {
  let db;
  let offers;

  beforeEach(async () => {
    db = await setup(mock.auth(), mock.collection.listings());
    offers = db.collection("offers");
  });

  afterEach(async () => teardown());

  test("Cannot add empty offer", async () => {
    await expect(offers.doc(`${mock.auth().uid}_lid`).set({})).toDeny();
  });

  test("Cannot add valid offer at bad ID", async () => {
    await expect(offers.add(mock.offer())).toDeny();
  });

  test("Can add valid offer", async () => {
    const ID = `${mock.auth().uid}_lid_item`;
    await expect(
      offers.doc(ID).set(mock.offer(true, mock.authOther(), mock.auth()))
    ).toAllow();
  });

  test("Can delete own offer", async () => {
    db = await setup(mock.auth(), mock.collection.offers());
    offers = db.collection("offers");

    const ID = `${mock.auth().uid}_lid_poke`;
    await expect(offers.doc(ID).delete()).toAllow();
  });
});
