const auth = admin => ({
  uid: "userID",
  name: "userName",
  email: "email@email.com",
  email_verified: true,
  admin: admin ? true : false
});

const authOther = admin => ({
  uid: "userID_other",
  name: "userName_other",
  email: "email_other@email.com",
  email_verified: true,
  admin: admin ? true : false
});

const user = _auth => ({
  uid: _auth ? _auth.uid : auth().uid,
  ign: _auth ? _auth.name : auth().name,
  store: null
});

const userOther = _auth => ({
  uid: _auth ? _auth.uid : authOther().uid,
  ign: _auth ? _auth.name : authOther().name,
  store: null
});

const listing = {
  item: _auth => ({
    uid: _auth ? _auth.uid : auth().uid,
    seller: _auth ? _auth.name : auth().name,
    name: "ITEM NAME",
    price: 1,
    status: {
      sold: false,
      offer_id: null
    }
  }),
  poke: _auth => ({
    uid: _auth ? _auth.uid : auth().uid,
    seller: _auth ? _auth.name : auth().name,
    name: "POKEMON NAME",
    price: 1,
    status: {
      sold: false,
      offer_id: null
    },
    details: {
      region: "region of origin",
      ot: "original trainer",
      nature: "nature",
      ability: "ability",
      hidden_power: "hidden power type",
      sex: true /* true == female */,
      ivs: {
        atk: 0,
        def: 0,
        spd: 0,
        spatk: 0,
        spdef: 0,
        hp: 0
      },
      evs: {
        atk: 0,
        def: 0,
        spd: 0,
        spatk: 0,
        spdef: 0,
        hp: 0
      }
    }
  })
};

const offer = (item, seller, buyer) => ({
  suid: seller ? seller.uid : auth().uid,
  buid: buyer ? buyer.uid : authOther().uid,
  lid: item ? "lid_item" : "lid_poke",
  name: item ? "ITEM NAME" : "POKEMON NAME",
  price: 1,
  ask: 1,
  status: {
    accepted: false,
    denied: false
  }
});

const store = _auth => ({
  name: "STORE NAME",
  description: "STORE DESCRIPTION",
  listings: [],
  sellers: [_auth ? _auth.uid : auth().uid],
  owner: {
    uid: _auth ? _auth.uid : auth().uid,
    ign: _auth ? _auth.name : auth().name
  }
});

const collection = {
  users: () => {
    const w = {};
    const a = [user(), userOther()];
    a.forEach(u => (w[`users/${u.uid}`] = u));
    return w;
  },
  listings: () => ({
    "listings/lid_poke": listing.poke(),
    "listings/lid_item": listing.item(authOther())
  }),
  offers: () => {
    const w = { ...collection.listings() };
    const a = [offer(true), offer(false, authOther(), auth())];
    a.forEach(o => (w[`offers/${o.buid}_${o.lid}`] = o));
    return w;
  },
  stores: () => {
    const w = {};
    const a = [store(), store(authOther())];
    a.forEach((s, i) => (w[`stores/store_${s.owner.uid}`] = s));
    return w;
  }
};

module.exports.mock = {
  auth: auth,
  authOther: authOther,
  user: user,
  userOther: userOther,
  listing: listing,
  offer: offer,
  store: store,
  collection: collection
};

const _nullify = obj => {
  const nulled = {};
  for (const k in obj) {
    nulled[k] =
      typeof obj[k] === "object"
        ? (nulled[k] = _nullify(obj[k]))
        : (nulled[k] = null);
  }
  return nulled;
};

module.exports.nullify = _nullify;
