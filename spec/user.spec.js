const { setup, teardown } = require("./helper");
const { mock } = require("./mock");

describe("No-Auth User rules", () => {
  let db;
  let users;

  beforeEach(async () => {
    db = await setup();
    users = db.collection("users");
  });

  afterEach(async () => teardown());

  test("Cannot list users", async () => await expect(users.get()).toDeny());

  test("Cannot add user", async () => {
    await expect(users.add(mock.user())).toDeny();
  });

  test("Cannot update user", async () => {
    db = await setup(null, mock.collection.users());
    users = db.collection("users");

    await expect(users.doc(mock.user().uid).update({})).toDeny();
  });

  test("Cannot delete user", async () => {
    db = await setup(null, mock.collection.users());
    users = db.collection("users");

    await expect(users.doc(mock.user().uid).delete()).toDeny();
  });

  test("Can get single user", async () => {
    db = await setup(null, mock.collection.users());
    users = db.collection("users");

    await expect(users.doc(mock.user().uid).get()).toAllow();
  });
});

describe("Auth User rules", () => {
  let db;
  let users;

  beforeEach(async () => {
    db = await setup(mock.auth());
    users = db.collection("users");
  });

  afterEach(async () => teardown());

  // Get

  test("Cannot list users", async () => await expect(users.get()).toDeny());

  test("Can get single user", async () => {
    db = await setup(mock.auth(), mock.collection.users());
    users = db.collection("users");

    await expect(users.doc(mock.user().uid).get()).toAllow();
  });

  // Create

  test("Cannot add user at random path", async () => {
    await expect(users.add(mock.user())).toDeny();
  });

  test("Cannot add user at incorrect path", async () => {
    await expect(users.doc("_").set(mock.user()));
  });

  test("Cannot add user with incorrect uid", async () => {
    await expect(users.doc(mock.user().uid).set({ ...mock.user(), uid: "_" }));
  });

  test("Cannot add user with empty fields", async () => {
    await expect(users.doc(mock.user().uid).set({}));
  });

  test("Cannot add user with missing uid", async () => {
    await expect(
      users.doc(mock.user().uid).set({ ign: mock.user().ign })
    ).toDeny();
  });

  test("Cannot add user with missing ign", async () => {
    await expect(
      users.doc(mock.user().uid).set({ uid: mock.user().uid })
    ).toDeny();
  });

  test("Cannot add user with extra fields", async () => {
    await expect(
      users.doc(mock.user().uid).set({ ...mock.user(), f: null })
    ).toDeny();
  });

  test("Cannot add user with ign longer than 120 char", async () => {
    await expect(
      users.doc(mock.user().uid).set({ ...mock.user(), ign: "_".repeat(121) })
    ).toDeny();
  });

  test("Cannot add user with incorrect ign", async () => {
    await expect(
      users.doc(mock.user().uid).set({ ...mock.user(), ign: "_" })
    ).toDeny();
  });

  test("Can add valid user", async () => {
    await expect(users.doc(mock.user().uid).set(mock.user())).toAllow();
  });

  // Mutate

  test("Cannot update non-self user", async () => {
    db = await setup(mock.auth(), mock.collection.users());
    users = db.collection("users");

    await expect(users.doc(mock.authOther().uid).update({ ign: "_" })).toDeny();
  });

  test("Cannot delete non-self user", async () => {
    db = await setup(mock.auth(), mock.collection.users());
    users = db.collection("users");

    await expect(users.doc(mock.userOther().uid).delete()).toDeny();
  });

  // TODO other mutate tests
});
