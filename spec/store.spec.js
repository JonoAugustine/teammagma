const { setup, teardown } = require("./helper");
const { mock } = require("./mock");

describe("Non auth store rules", () => {
  let db;
  let stores;

  beforeEach(async () => {
    db = await setup();
    stores = db.collection("stores");
  });

  afterEach(async () => await teardown());

  test("Can read stores", async () => await expect(stores.get()).toAllow());

  test("Cannot add store", async () => {
    await expect(stores.add(mock.store())).toDeny();
  });
});

describe("Auth store rules", () => {
  let db;
  let stores;

  beforeEach(async () => {
    db = await setup(mock.auth(), mock.collection.users());
    stores = db.collection("stores");
  });

  afterEach(async () => await teardown());

  test("Can add store ", async () => {
    await expect(stores.add(mock.store())).toAllow();
  });

  test("Can update own store name", async () => {
    db = await setup(mock.auth(), mock.collection.stores());
    stores = db.collection("stores");

    await expect(
      stores.doc(`store_${mock.auth().uid}`).update({ name: "abc" })
    ).toAllow();
  });

  // TODO more owner rules & seller rules
});
