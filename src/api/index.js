import * as Routes from "./routes";
import { getWindowSize } from "./window";
import { api, statShorthand } from "./PokeApi";

const formatNumber = num =>
  num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");

export { Routes, getWindowSize, api as PokeApi, statShorthand, formatNumber };
