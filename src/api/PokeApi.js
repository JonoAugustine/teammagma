const PokeApi = require("pokeapi-js-wrapper");

const api = new PokeApi.Pokedex();

const statShorthand = name => {
  switch (name) {
    case "attack":
      return "atk";
    case "defense":
      return "def";
    case "speed":
      return "spd";
    case "special_attack":
      return "spatk";
    case "special_defense":
      return "spdef";
    default:
      // "health":
      return "hp";
  }
};

export { api, statShorthand };
