import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/firebase-storage";
import "firebase/firebase-messaging";
import "firebase/firebase-functions";

const firebaseConfig = {
  apiKey: "AIzaSyBCrowVat7vJ1gsOQ9ztsNKRGvc0uel3u0",
  authDomain: "teammagmastore-aae1c.firebaseapp.com",
  databaseURL: "https://teammagmastore-aae1c.firebaseio.com",
  projectId: "teammagmastore-aae1c",
  storageBucket: "teammagmastore-aae1c.appspot.com",
  messagingSenderId: "338192746726",
  appId: "1:338192746726:web:c38f7a69d6019345079327"
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();

const firestore = firebase.firestore();
firestore.enablePersistence();

const storage = firebase.storage();

const functions = firebase.functions();

const messaging = firebase.messaging();

export { auth, firestore, storage, messaging, functions };
