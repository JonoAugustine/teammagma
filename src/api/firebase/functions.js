import { functions } from "./firebase";

const setAdminRole = functions.httpsCallable("setRoleAdmin");

export const setAdminStatus = (uid, email, isAdmin) =>
  setAdminRole({
    uid: uid,
    email: email,
    admin: isAdmin
  });

const deny_offer = functions.httpsCallable("deny_offer");

export const denyOffer = (lid, offer_id) =>
  deny_offer({ lid: lid, offer_id: offer_id });

const ban_user = functions.httpsCallable("ban_user");

export const banUser = (uid, email) => ban_user({ uid: uid, email: email });

const create_store = functions.httpsCallable("create_store");

export const createStore = name => create_store({ name: name });

const transfer_store_ownership = functions.httpsCallable(
  "transfer_store_ownership"
);

export const transferStoreOwnership = (store_id, uid) => {
  return transfer_store_ownership({ store_id: store_id, uid: uid });
};

const add_listing_to_store = functions.httpsCallable("add_listing_to_store");

export const addListingToStore = (store_id, lid) => {
  return add_listing_to_store({ store_id: store_id, lid: lid });
};

const remove_listing_from_store = functions.httpsCallable(
  "remove_listing_from_store"
);

export const removeListingFromStore = (store_id, lid) => {
  return remove_listing_from_store({ store_id: store_id, lid: lid });
};
