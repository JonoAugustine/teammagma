export class CollectionView {
  //
  collection;
  query = null;
  filter = null;
  pageSize = 15;
  started = false;
  defaultFilters = [];

  docs = [];

  constructor(collection, pageSize, ...filters) {
    this.collection = collection;
    if (typeof pageSize === "number") {
      this.pageSize = pageSize;
    }
    this.defaultFilters = filters;
  }

  start = async (...additionalFilters) => {
    this.query = this.collection;

    [...additionalFilters, ...this.defaultFilters].forEach(
      f => (this.query = this.query.where(...f))
    );

    const snap = await this.query.get();
    this.docs = snap.docs;
    this.started = true;
    return this.docs;
  };

  nextPage = async () => {
    if (this.query === null) this.start();
    const nextSnap = await this.query
      .startAfter(this.docs[this.docs.length - 1])
      .get();
    this.docs = [...this.docs, ...nextSnap.docs];
    return this.docs;
  };

  reset = () => {
    this.docs = [];
    this.started = false;
  };
}
