import { firestore } from "firebase/app";
import * as FB from "./firebase";

export const users = FB.firestore.collection("users");

export const getUser = uid => users.doc(uid).get();

FB.auth.onAuthStateChanged(user => {
  if (user === null) return;
  users.doc(user.uid).onSnapshot(snap => {
    if (!snap.exists) {
      if (user.displayName === null) {
        window.location.href = "/profile/settings";
      } else {
        users
          .doc(FB.auth.currentUser.uid)
          .set(
            { uid: user.uid, ign: FB.auth.currentUser.displayName },
            { merge: true }
          );
      }
    }
  });
});

export const updateIGN = async ign => {
  await FB.auth.currentUser.updateProfile({ displayName: ign });
  await users
    .doc(FB.auth.currentUser.uid)
    .set({ uid: FB.auth.currentUser.uid, ign: ign }, { merge: true });
};

export const createUser = async (email, password, ign) => {
  const cred = await FB.auth.createUserWithEmailAndPassword(email, password);

  await cred.user.updateProfile({ displayName: ign });

  await users
    .doc(cred.user.uid)
    .set({ uid: cred.user.uid, ign: ign, store: null });

  sendVerify();

  return FB.auth.currentUser;
};

export const userLogin = async (email, password) => {
  const cred = await FB.auth.signInWithEmailAndPassword(email, password);
  const doc = await users.doc(cred.user.uid).get();
  if (!doc.exists) {
    try {
      await doc.ref.set({ uid: cred.user.uid, ign: cred.user.displayName });
    } catch (err) {
      console.log("Failed to update db on login", err);
    }
  }

  return cred.user;
};

export const userLogout = () => FB.auth.signOut();

export const sendVerify = () => FB.auth.currentUser.sendEmailVerification();

export const sendPasswordReset = email => FB.auth.sendPasswordResetEmail(email);

export const deleteSelf = () => FB.auth.currentUser.delete();

export const listings = FB.firestore.collection("listings");

export const createListing = (name, price, details) => {
  const n = {
    uid: FB.auth.currentUser.uid,
    seller: FB.auth.currentUser.displayName,
    name: name,
    price: parseInt(price),
    status: {
      sold: false,
      offer_id: null
    }
  };
  if (details) n["details"] = details;
  console.log("Attempting to create new listing", n);
  return listings.add(n);
};

export const acceptOffer = (lid, offerID) => {
  return listings.doc(lid).update({ "status.offer_id": offerID });
};

export const offers = FB.firestore.collection("offers");

export const getAllOffers = async () => {
  const res = await Promise.all([
    offers
      .where("suid", "==", FB.auth.currentUser.uid)
      .get()
      .then(snap => snap.docs),
    offers
      .where("buid", "==", FB.auth.currentUser.uid)
      .get()
      .then(snap => snap.docs)
  ]);
  return [].concat.apply([], res);
};

export const getListingOffers = listingID => {
  return offers.where("lid", "==", listingID).get();
};

export const createOffer = (lid, suid, name, ask, price) => {
  if (suid === FB.auth.currentUser.uid) {
    return new Error("Cannot make offer for own listing");
  }
  const o = {
    buid: FB.auth.currentUser.uid,
    suid: suid,
    lid: lid,
    name: name,
    ask: ask,
    price: parseInt(price),
    status: {
      accepted: false,
      denied: false
    }
  };
  const ID = `${FB.auth.currentUser.uid}_${lid}`;
  console.log("Attempting to create offer at", ID, o);
  return offers.doc(ID).set(o);
};

export const updateOffer = async (lid, newPrice) => {
  return await offers
    .doc(`${FB.auth.currentUser.uid}_${lid}`)
    .set({ price: newPrice });
};

export const deleteOffer = async lid => {
  return await offers.doc(`${FB.auth.currentUser.uid}_${lid}`).delete();
};

export const stores = FB.firestore.collection("stores");

export const updateStore = (store_id, data) => {
  return stores.doc(store_id).update(data);
};

export const addStoreSeller = async (store_id, uid) => {
  await updateStore(store_id, {
    listings: firestore.FieldValue.arrayUnion(uid)
  });
};

export const deleteStore = store_id => stores.doc(store_id).delete();

export const news = FB.firestore.collection("news");
