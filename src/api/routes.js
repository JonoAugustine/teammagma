export const home = "/";
export const about = "/magma";

export const stores = "/store";
export const storePage = stores + "/:store_id";
export const storeSettings = id => `${stores}/${id}/settings`;

export const profile = "/profile";
export const createListing = profile + "/createlisting";
export const profileSettings = profile + "/settings";

export const admin = profile + "/admin";
