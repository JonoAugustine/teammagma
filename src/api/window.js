const rangeOf = (min, max) => ({ min: min, max: max });

const inRange = (int, range) => int >= range.min && int <= range.max;

const xs = rangeOf(0, 575.98);

const sm = rangeOf(576, 767.98);

const md = rangeOf(768, 991.98);

const lg = rangeOf(992, 1199.98);

//const xl = rangeOf(1200, 999999);

export const getWindowSize = () => {
  const w = window.innerWidth;
  if (inRange(w, xs)) {
    return "xs";
  } else if (inRange(w, sm)) {
    return "sm";
  } else if (inRange(w, md)) {
    return "md";
  } else if (inRange(w, lg)) {
    return "lg";
  } else return "xl";
};
