import React from "react";
import PropTypes from "prop-types";

import { Button, Modal, ModalBody, ModalHeader } from "reactstrap";

import {
  AvForm,
  AvCheckboxGroup,
  AvCheckbox
} from "availity-reactstrap-validation";

const ReportModal = props => {
  const sendReport = (_, values) => {
    // TODO
  };

  if (props.data === null || typeof props.data["options"] === "undefined")
    return <div></div>;

  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle}>
      <ModalHeader>Report {props.data.header}</ModalHeader>
      <ModalBody>
        <AvForm onValidSubmit={sendReport}>
          <AvCheckboxGroup name="Report Options">
            {props.data.options.map((o, i) => (
              <AvCheckbox key={i} label={o} name={o} value={o} />
            ))}
          </AvCheckboxGroup>
          <Button color="danger">Submit Report</Button>
        </AvForm>
      </ModalBody>
    </Modal>
  );
};

ReportModal.propTypes = {
  isOpen: PropTypes.bool,
  toggle: PropTypes.func,
  data: PropTypes.object
};

export default ReportModal;
