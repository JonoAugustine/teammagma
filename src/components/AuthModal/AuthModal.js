import React, { useState, useContext } from "react";

import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Row,
  Col
} from "reactstrap";
import { AvForm, AvField } from "availity-reactstrap-validation";

import { NotificationCtx } from "../../contexts";

import "./authmodal.css";
import { dao } from "../../api/firebase";

const ResetBody = props => {
  const { notify } = useContext(NotificationCtx);
  return (
    <React.Fragment>
      <ModalHeader>Password Reset</ModalHeader>
      <ModalBody>
        <AvForm
          onValidSubmit={(_, values) => {
            dao
              .sendPasswordReset(values.email)
              .then(() => {
                notify(
                  "green",
                  `Check ${values.email} to complete your password reset`
                );
                props.toggle();
              })
              .catch(err => {
                console.log(err);
                notify(
                  "danger",
                  "An error ocurred, please double check the provided email."
                );
              });
          }}
        >
          <AvField name="email" type="email" label="Email" required />
          <Button color="warning">Send Email Reset</Button>
        </AvForm>
      </ModalBody>
    </React.Fragment>
  );
};

const SignupBody = props => {
  const { notify } = useContext(NotificationCtx);

  const signup = (_, values) => {
    dao.createUser(values.email, values.password, values.name).then(() => {
      props.toggle();
      notify(
        "success",
        <Row className="justify-content-center">
          <div style={{ float: "left" }}>
            Welcome to the Team Magma Store! Please check your email to verify
            your account.
          </div>
          <Button
            style={{ margin: "0 1em", float: "right" }}
            onClick={() => {
              window.location.href = "/account";
            }}
          >
            Click to view your account. Ignore to keep on browsing.
          </Button>
        </Row>
      );
    });
  };

  return (
    <React.Fragment>
      <ModalHeader>Signup</ModalHeader>
      <ModalBody>
        <AvForm onValidSubmit={signup}>
          <AvField name="email" type="email" label="Email" required />
          <AvField name="name" type="text" label="In Game Name" required />
          <AvField
            name="password"
            type="text"
            label="Password"
            validate={{
              required: { value: true },
              minLength: { value: 6 }
            }}
          />
          <Button color="primary">Singup</Button>
        </AvForm>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.openLogin}>
          Already Have an Account?
        </Button>
      </ModalFooter>
    </React.Fragment>
  );
};

const LoginBody = props => {
  const { notify } = useContext(NotificationCtx);
  
  const login = (_, values) => {
    dao
      .userLogin(values.email, values.password)
      .then(user => notify("success", `Welcome back ${user.displayName}`))
      .catch(err => notify("danger", `An err occurred. "${err.message}"`))
      .finally(() => props.toggle());
  };

  return (
    <React.Fragment>
      <ModalHeader>Login</ModalHeader>
      <ModalBody>
        <AvForm onValidSubmit={login}>
          <AvField name="email" label="Email" type="email" required />
          <AvField name="password" label="Password" type="password" required />
          <Button color="primary">Login</Button>
        </AvForm>
      </ModalBody>
      <ModalFooter>
        <Row style={{ margin: "0", padding: "0" }}>
          <Col style={{ padding: "0" }}>
            <Button
              color="primary"
              onClick={props.openSignup}
              style={{ display: "block", marginBottom: "1em" }}
            >
              Don't Have an Account?
            </Button>
            <Button
              color="warning"
              onClick={props.openReset}
              style={{ display: "block" }}
            >
              Forgot Your Password?
            </Button>
          </Col>
        </Row>
      </ModalFooter>
    </React.Fragment>
  );
};

const AuthModal = props => {
  const [signupOpen, setSignupOpen] = useState(
    props.signupOpen ? true : !(props.resetOpen || props.loginOpen)
  );
  const [loginOpen, setLoginOpen] = useState(props.loginOpen ? true : false);

  const openSignup = () => {
    setSignupOpen(true);
    setLoginOpen(false);
  };

  const openLogin = () => {
    setLoginOpen(true);
    setSignupOpen(false);
  };

  const openReset = () => {
    setLoginOpen(false);
    setSignupOpen(false);
  };

  const getContents = () => {
    if (loginOpen)
      return (
        <LoginBody
          openReset={openReset}
          openSignup={openSignup}
          modalOpen={props.isOpen}
          toggle={props.toggle}
        />
      );
    else if (signupOpen)
      return (
        <SignupBody
          openLogin={openLogin}
          modalOpen={props.isOpen}
          toggle={props.toggle}
        />
      );
    else
      return (
        <ResetBody
          openLogin={openLogin}
          modalOpen={props.isOpen}
          toggle={props.toggle}
        />
      );
  };

  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle} unmountOnClose={false}>
      {getContents()}
    </Modal>
  );
};

export default AuthModal;
