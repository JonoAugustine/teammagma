import React, { useState, useContext } from "react";
import PropTypes from "prop-types";

import {
  Container,
  Row,
  Col,
  Collapse,
  Button,
  Modal,
  ModalBody,
  ModalHeader
} from "reactstrap";
//import { AvField, AvForm } from "availity-reactstrap-validation";
import { Section, SubSection } from "./Dashboard";
import { CollectionView, dao, functions } from "../../api/firebase";
import { NotificationCtx } from "../../contexts";

const UserCard = props => {
  const { notify } = useContext(NotificationCtx);
  const [isOpen, setIsOpen] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);

  const ban = () => {
    functions
      .banUser(props.userDoc.id)
      .then(resp => {
        notify(
          resp.data.status > 400 ? "danger" : "success",
          resp.data.message
        );
        props.refresh();
        setIsOpen(false);
        setModalOpen(false);
      })
      .catch(err => {
        notify("warning", `An err occurred ${err.message}`);
        setIsOpen(false);
        setModalOpen(false);
      });
  };

  return (
    <Container
      fluid
      style={{
        border: "1px solid red",
        borderRadius: "10px",
        padding: "0.5em",
        margin: "0.3em"
      }}
      onClick={() => setIsOpen(!isOpen)}
    >
      <Row>
        <Col>{props.ign}</Col>
      </Row>
      <Collapse isOpen={isOpen}>
        <Button color="danger" onClick={() => setModalOpen(true)}>
          Ban
        </Button>
      </Collapse>
      <Modal isOpen={modalOpen} toggle={() => setModalOpen(!modalOpen)}>
        <ModalHeader>Ban user {props.ign}?</ModalHeader>
        <ModalBody style={{ display: "inline-flex" }}>
          <Button color="danger" style={{ margin: "auto" }} onClick={ban}>
            Yes
          </Button>
          <Button
            color="primary"
            style={{ margin: "auto" }}
            onClick={() => setModalOpen(false)}
          >
            No
          </Button>
        </ModalBody>
      </Modal>
    </Container>
  );
};

UserCard.propTypes = {
  userDoc: PropTypes.object.isRequired,
  //email: PropTypes.string.isRequired,
  ign: PropTypes.string.isRequired,
  refresh: PropTypes.func.isRequired
};

const userView = new CollectionView(dao.users, 10);

const Admin = props => {
  const [userDocs, setUserDocs] = useState([]);

  const refreshUsers = () => {
    userView.reset();
    setUserDocs([]);
  };

  if (!userView.started) {
    userView.start().then(setUserDocs);
  }

  return (
    <Container fluid>
      <Row>
        <Col>
          <Section header="Users">
            <SubSection>
              {userDocs.map(ud => (
                <UserCard
                  userDoc={ud}
                  ign={ud.data().ign}
                  //email={ud.data().email}
                  refresh={refreshUsers}
                  key={ud.id}
                />
              ))}
            </SubSection>
          </Section>
        </Col>
      </Row>
    </Container>
  );
};

export default Admin;
