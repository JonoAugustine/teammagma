import React, { useState, useContext } from "react";

import { Container, Row, Col, Card, Collapse, Button } from "reactstrap";
import { ListingCard } from "../Listing";

import { AuthenticationCtx, NotificationCtx } from "../../contexts";
import { dao, CollectionView, functions } from "../../api/firebase";
import { formatNumber } from "../../api";

const OfferCard = props => {
  const { notify } = useContext(NotificationCtx);
  const { user } = useContext(AuthenticationCtx);
  const [data, setData] = useState(props.doc.data());
  const [sellerName, setSellerName] = useState("");
  const [buyerName, setbuyerName] = useState("");
  const [idOpen, setIdOpen] = useState(false);

  if (!sellerName) {
    if (data.suid === user.uid) setSellerName(user.displayName);
    else
      dao
        .getUser(data.suid)
        .then(doc => setSellerName(doc.data() ? doc.data().ign : "*err*"))
        .catch(() => setSellerName("*err*"));
  }
  if (!buyerName) {
    if (data.buid === user.uid) setbuyerName(user.displayName);
    else dao.getUser(data.buid).then(doc => setbuyerName(doc.data().ign));
  }

  const acceptOffer = () => {
    dao
      .acceptOffer(data.lid, props.doc.id)
      .then(() => {
        notify("success", <div>The offer has been accepted</div>);
      })
      .catch(err =>
        notify("danger", <div>An err occurred. {err.message}</div>)
      );
  };
  const denyOffer = () => {
    functions
      .denyOffer(data.lid, props.doc.id)
      .then(resp => {
        notify(
          resp.data.status > 400 ? "warning" : "success",
          <div>{resp.data.message}</div>
        );
        props.refresh();
      })
      .catch(err =>
        notify("danger", <div>An err occurred. "{err.message}"</div>)
      );
  };

  const cancelOffer = () => {
    dao
      .deleteOffer(data.lid)
      .then(() => {
        notify("success", <div>The offer has been canceled</div>);
        props.refresh();
      })
      .catch(err => {
        notify("danger", <div>An err occurred. {err.message}</div>);
        console.log(err);
      });
  };

  return (
    <a style={{ cursor: "pointer" }} onClick={() => setIdOpen(!idOpen)}>
      <Card
        style={{
          backgroundColor: "transparent",
          border: `1px solid ${
            data.denied ? "orange" : data.accepted ? "green" : "red"
          }`,
          padding: "1em",
          marginBottom: "0.5em",
          color: "white"
        }}
      >
        <Row>
          <Col>
            <Row>
              <Col xs={12} sm={6}>
                <Row style={{ margin: 0 }}>{`name: ${data.name}`}</Row>
                <Row style={{ margin: 0 }}>
                  {props.seller
                    ? `buyer: ${buyerName}`
                    : `seller: ${sellerName}`}
                </Row>
              </Col>
              <Col xs={12} sm={6}>
                <Row style={{ margin: 0 }}>
                  {`Ask: ${formatNumber(data.ask / 1000)}K`}
                </Row>
                <Row style={{ margin: 0 }}>
                  {`offer: ${formatNumber(data.price / 1000)}K`}
                </Row>
              </Col>
            </Row>
            <Collapse isOpen={idOpen}>
              <Row>
                <Col>
                  {props.seller
                    ? `seller: ${sellerName}`
                    : `buyer: ${buyerName}`}
                </Col>
                <Col sm={12}>LID: {data.lid}</Col>
              </Row>
              <Row style={{ marginTop: "0.5em" }}>
                {data.suid === user.uid ? (
                  <React.Fragment>
                    <Button
                      outline
                      color="danger"
                      onClick={denyOffer}
                      style={{ margin: "auto" }}
                    >
                      Deny Offer
                    </Button>
                    <Button
                      color="success"
                      onClick={acceptOffer}
                      style={{ margin: "auto" }}
                    >
                      Accept Offer
                    </Button>
                  </React.Fragment>
                ) : (
                  <Button
                    outline
                    color="danger"
                    style={{ margin: "auto 0 auto auto" }}
                    onClick={cancelOffer}
                    style={{ margin: "auto" }}
                  >
                    Cancel Offer
                  </Button>
                )}
              </Row>
            </Collapse>
          </Col>
        </Row>
      </Card>
    </a>
  );
};

export const Section = props => {
  return (
    <Row style={{ margin: "0 0 1.5em 1em" }}>
      <Col style={{ padding: 0 }}>
        <h4>{props.header}</h4>
        {props.children}
      </Col>
    </Row>
  );
};

export const SubSection = props => {
  return (
    <Col
      sm={props.sm ? props.sm : 12}
      md={props.md ? props.md : 6}
      xl={props.xl ? props.xl : 4}
      style={{
        margin: 0,
        overflowY: "auto",
        scrollbarWidth: "none",
        maxHeight: "69vh",
        ...props.style
      }}
    >
      <h5>{props.header}</h5>
      {props.children}
    </Col>
  );
};

const Dashboard = () => {
  const { user } = useContext(AuthenticationCtx);

  const [offerView_sent, _] = useState(new CollectionView(dao.offers));
  const [offerView_rec, __] = useState(new CollectionView(dao.offers));
  const [offerDocs_sent, setOfferDocs_sent] = useState([]);
  const [offerDocs_rec, setOfferDocs_rec] = useState([]);
  const [listingsView, ___] = useState(new CollectionView(dao.listings));
  const [listingDocs, setListingDocs] = useState([]);

  if (user === null) return <div></div>;

  if (!listingsView.started) {
    listingsView.start(["uid", "==", user.uid]).then(setListingDocs);
  }

  if (!offerView_sent.started) {
    offerView_sent.start(["buid", "==", user.uid]).then(setOfferDocs_sent);
  }

  if (!offerView_rec.started) {
    offerView_rec.start(["suid", "==", user.uid]).then(setOfferDocs_rec);
  }

  const refreshOffers = () => {
    offerView_sent.reset();
    offerView_rec.reset();
    setOfferDocs_sent([]);
    setOfferDocs_rec([]);
  };

  return (
    <Container fluid>
      <Row>
        <Col>
          <Section header="Offers">
            <Row style={{ margin: 0 }}>
              <SubSection header="Received">
                {offerDocs_rec
                  .filter(o => o.data().suid === user.uid && !o.data().denied)
                  .map(o => (
                    <OfferCard
                      key={o.id}
                      doc={o}
                      refresh={refreshOffers}
                      seller
                    />
                  ))}
              </SubSection>
              <SubSection header="Sent">
                {offerDocs_sent
                  .filter(o => o.data().buid === user.uid)
                  .sort((a, b) =>
                    a.data().accepted
                      ? 1
                      : b.data().accepted
                      ? -1
                      : !a.data().denied
                      ? 1
                      : !b.data().denied
                      ? -1
                      : 0
                  )
                  .map(o => (
                    <OfferCard key={o.id} doc={o} refresh={refreshOffers} />
                  ))}
              </SubSection>
              <SubSection header="Active Listings">
                {listingDocs.map(d => (
                  <ListingCard
                    noImage
                    noSeller
                    hideModals
                    key={d.id}
                    doc={d}
                    data={d.data()}
                  />
                ))}
              </SubSection>
            </Row>
          </Section>
        </Col>
      </Row>
    </Container>
  );
};

export default Dashboard;
