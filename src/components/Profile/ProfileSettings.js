import React, { useState, useContext } from "react";

import { Container, Row, Col, Button } from "reactstrap";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { AuthenticationCtx, NotificationCtx } from "../../contexts";
import { dao, auth } from "../../api/firebase";

const ProfileSettings = () => {
  const { user } = useContext(AuthenticationCtx);
  const { notify } = useContext(NotificationCtx);

  const verify = () => {
    dao
      .sendVerify()
      .then(() => notify("success", <div>Please check your email.</div>))
      .catch(() =>
        notify(
          "danger",
          <div>Something went wrong, please try again later.</div>
        )
      );
  };

  const updateProfile = (_, values) => {
    dao
      .updateIGN(values.ign)
      .then(() => notify("success", "IGN updated"))
      .then(() => auth.currentUser.updateEmail(values.email))
      .then(() => notify("success", "Email updated"))
      .catch(err => notify("danger", "Failed to save settings"));
  };

  if (user === null) return <div></div>;

  return (
    <Container fluid>
      <Row>
        <Col>
          Email Verified:{" "}
          {user.emailVerified ? (
            "Verified"
          ) : (
            <a
              style={{ color: "orange", cursor: "pointer" }}
              onClick={() => verify()}
            >
              Unverified
            </a>
          )}
          <AvForm
            onValidSubmit={updateProfile}
            model={{
              ign: auth.currentUser.displayName
                ? auth.currentUser.displayName
                : "",
              email: auth.currentUser.email
            }}
          >
            <AvField
              type="text"
              name="ign"
              label="In Game Name (IGN)"
              validate={{
                required: {
                  value: true,
                  errorMessage:
                    "You will not be able to send offers or make listings without and IGN"
                }
              }}
            />
            <AvField type="email" name="email" label="Email" />
            <Button type="submit" color="success">
              Save Changes
            </Button>
          </AvForm>
        </Col>
      </Row>
    </Container>
  );
};

export default ProfileSettings;
