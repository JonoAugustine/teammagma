import Home from "./Home";
import Stores from "./Stores";
import TeamMagma from "./TeamMagma";
import Profile from "./Profile";

export { Home, Profile, TeamMagma, Stores };
