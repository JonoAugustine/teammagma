import React from "react";

import { Row, Col, Container } from "reactstrap";

import { ListingSection } from "../Listing";
import { News } from "../News";
import { CollectionView, dao } from "../../api/firebase";

const Home = () => {
  const cv = new CollectionView(dao.listings);
  return (
    <Container>
      <Row style={{ textAlign: "center" }} className="justify-content-center">
        <Col lg={12}>
          <h5 style={{ fontSize: "5vh" }}>Team Magma</h5>
        </Col>
      </Row>
      <ListingSection default listingsView={cv} />
    </Container>
  );
};

export default Home;
