import React, { useEffect, useState, useContext } from "react";

import { BrowserRouter as Router, Route } from "react-router-dom";

import { Container, Col, Row, Button } from "reactstrap";

import { AuthenticationCtx, WindowSizeCtx } from "../../contexts";
import { Sidebar, SidebarItem, SidebarNav } from "../Navigation";
import { Routes } from "../../api";
import { ListingCreation } from "../Listing";
import { ProfileSettings, Dashboard, AdminPanel } from "../Profile";

import rootStyle from "../../style/App.css";

const Profile = () => {
  const { user, isAdmin } = useContext(AuthenticationCtx);
  const { windowSize } = useContext(WindowSizeCtx);
  const [sidebarOpen, setSidebarOpen] = useState(true);

  const toggleSidebar = () => setSidebarOpen(!sidebarOpen);

  // Hide & show on window size change
  useEffect(() => {
    if (windowSize === "xl" || windowSize === "lg") {
      setSidebarOpen(true);
    } else {
      setSidebarOpen(false);
    }
  }, [windowSize]);

  const sidebarToggler = () => {
    const getStyle = () => {
      if (sidebarOpen)
        return {
          display: "none"
        };
      else
        return {
          color: "white"
        };
    };
    return windowSize !== "lg" && windowSize !== "xl" ? (
      <Button color="none" onClick={toggleSidebar} style={getStyle()}>
        =>
      </Button>
    ) : (
      ""
    );
  };

  if (!user) return <div></div>; // used to wait for user to load

  return (
    <Container fluid>
      <Row>
        <Col>
          <Row>
            <Col>
              <Row
                style={{
                  backgroundColor: "darkred",
                  marginBottom: "1em"
                }}
              >
                {sidebarToggler()}
                <Col xs={6}>
                  <a
                    href={Routes.profile}
                    style={rootStyle}
                    className="header-link"
                  >
                    <h1>{user.displayName}</h1>
                  </a>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Sidebar isOpen={sidebarOpen} toggle={toggleSidebar}>
              <SidebarNav>
                <SidebarItem href={Routes.profile}>Dashboard</SidebarItem>
                <SidebarItem href={Routes.createListing}>
                  Create Listing
                </SidebarItem>
                <SidebarItem href={Routes.profileSettings}>
                  Edit Profile
                </SidebarItem>
                {isAdmin ? (
                  <SidebarItem href={Routes.admin}>Admin Panel</SidebarItem>
                ) : (
                  ""
                )}
              </SidebarNav>
            </Sidebar>
            <Col>
              <Row>
                <Col>
                  <Router>
                    <Route exact path={Routes.profile} component={Dashboard} />
                    <Route
                      path={Routes.createListing}
                      component={ListingCreation}
                    />
                    <Route
                      path={Routes.profileSettings}
                      component={ProfileSettings}
                    />
                    <Route path={Routes.admin} component={AdminPanel} />
                  </Router>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

export default Profile;
