import React, { useState } from "react";
import PropTypes from "prop-types";

import {
  Container,
  Col,
  Row,
  Button,
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from "reactstrap";

import {
  AvForm,
  AvField,
  AvCheckboxGroup,
  AvCheckbox
} from "availity-reactstrap-validation";

import { WindowSizeCtx, AuthenticationCtx } from "../../contexts";

import MagmaLogo from "../../images/magma_logo.png";

import TeamPhoto_One from "../../images/magma/event_1.png";
import TeamPhoto_Two from "../../images/magma/event_2.png";
import TeamPhoto_Three from "../../images/magma/event_3.png";
import TeamPhoto_Four from "../../images/magma/event_4.png";
import TeamPhoto_Five from "../../images/magma/event_5.png";
import HopeYouJoin from "../../images/magma/hope_you_join.png";

import style from "./pages.css";

const items = [
  {
    src: TeamPhoto_One,
    altText: "Slide 1"
    //caption: "Slide 1"
  },
  {
    src: TeamPhoto_Two,
    altText: "Slide 2"
    //caption: "Slide 2 "
  },
  {
    src: TeamPhoto_Three,
    altText: "Slide 3"
    //caption: "Slide 3 "
  },
  {
    src: TeamPhoto_Four,
    altText: "Slide 3"
    //caption: "Slide 3 "
  },
  {
    src: TeamPhoto_Five,
    altText: "Slide 5"
    //caption: "Slide 3 "
  }
];

const Slides = () => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };

  const goToIndex = newIndex => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  const slides = items.map(item => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img src={item.src} alt={item.altText} />
        <CarouselCaption
          captionText={item.caption}
          captionHeader={item.caption}
        />
      </CarouselItem>
    );
  });

  return (
    <Carousel activeIndex={activeIndex} next={next} previous={previous}>
      <CarouselIndicators
        items={items}
        activeIndex={activeIndex}
        onClickHandler={goToIndex}
      />
      {slides}
      <CarouselControl
        direction="prev"
        directionText="Previous"
        onClickHandler={previous}
      />
      <CarouselControl
        direction="next"
        directionText="Next"
        onClickHandler={next}
      />
    </Carousel>
  );
};

const TeamMagma = props => {
  const { user } = React.useContext(AuthenticationCtx);

  const submit = (_, values) => {
    // TODO join form submissions
  };

  return (
    <Container style={style} id="team_magma_page">
      <Row>
        <Col className="justify-content-center">
          <Row>
            <Col lg={12} xl={6}>
              <img
                style={{ maxWidth: "50%" }}
                src={MagmaLogo}
                alt="Magma Logo"
                className="logo"
              />
            </Col>
            <Col lg={12} xl={6}>
              <h1 id="header-one">Team Magma</h1>
              <p>
                Team Magma strives to provide a friendly & cooperative
                environment for its members. We are relatively casual, with an
                emphasis on community rather than PvP. Despite this, we are
                usually able to get in the Top 10 Guild Ladder. Our guild-chat
                and Discord are both active all day long with members from
                across the globe, and we do our best to address any questions or
                concerns you might have.
              </p>
            </Col>
          </Row>
          <hr />
          <Row>
            <Col md={6} lg={12}>
              <h2>Events</h2>
              <p>
                In order to bring members closer we hold several different types
                of events including guild hunts, tournaments, and hide and
                seeks. We do our best to make the events fun for all who wish to
                participate and also provide worthwhile rewards.
              </p>
            </Col>
            <Col md={6} lg={6}>
              <Slides />
            </Col>
            <Col md={12} lg={6}>
              <h2>Discord</h2>
              <p>
                On top of having an active guild chat we also have a Discord,
                though joining it is not mandatory! If for some reason you are
                not able to get help in guild chat, you are free to ask there.
                We often use voice chat, and even now and then have music
                sessions or play something together that isn't PRO.
              </p>
            </Col>
          </Row>
          <hr />
          <h2>What We Need</h2>
          <Row>
            <Col xs={12} md={4}>
              <h5>BE ACTIVE</h5>
              <p>
                We take activity very seriously, be sure to tell when you will
                be inactive so that you won't get kicked!
              </p>
            </Col>
            <Col xs={12} md={4}>
              <h5>Be nice and friendly</h5>
              <p>
                This should be obvious, but we aim for a nice community of
                friends so it is something we stress.
              </p>
            </Col>
            <Col xs={12} md={4}>
              <h5>Be Mature, But Humorus</h5>
              <p>
                We want everyone to have fun but you need to know when things
                are going too far.
              </p>
            </Col>
          </Row>
          <hr />
          <h4>Apply for Team Magma</h4>
          <AvForm
            onValidSubmit={submit}
            model={{
              name: user ? user.displayName : ""
            }}
          >
            <AvField
              name="ign"
              label="What is your in-game name?"
              type="text"
              required
            />
            <Row>
              <Col xs={12} md={6} xl={3}>
                <AvField
                  name="age"
                  label="How old are you?"
                  type="number"
                  validate={{
                    required: { value: true },
                    min: { value: 10 },
                    max: { value: 120 }
                  }}
                />
              </Col>
              <Col xs={12} md={6} xl={3}>
                <AvField
                  name="hours"
                  label="How many hours have you played?"
                  type="number"
                  validate={{
                    required: { value: true },
                    min: { value: 0 }
                  }}
                />
              </Col>
              <Col xs={12} md={6} xl={3}>
                <AvField
                  name="hours"
                  label="How many badges do you have?"
                  type="number"
                  validate={{
                    required: { value: true },
                    min: { value: 0 },
                    max: { value: 64 }
                  }}
                />
              </Col>
              <Col xs={12} md={6} xl={3}>
                <Row>
                  <Col xs={8} style={{ textAlign: "right" }}>
                    <label>Do you use Discord?</label>
                  </Col>
                  <Col xs={1}>
                    <AvCheckboxGroup name="discord" inline>
                      <AvCheckbox name="discord" value="yes" />
                    </AvCheckboxGroup>
                  </Col>
                </Row>
              </Col>
            </Row>
            <label>Favorite Playstyles</label>
            <AvField
              name="playstyle"
              label="(farming, selling, pvp, collecting, etc)"
              type="text"
              required
            />
            <AvField
              name="why"
              label="Why do you want to join Team Magma?"
              type="textarea"
              required
            />
            <Button type="submit" color="danger">
              Join Magma
            </Button>
          </AvForm>
          <img
            style={{ maxWidth: "80%" }}
            alt="Hope You Join"
            src={HopeYouJoin}
          />
        </Col>
      </Row>
    </Container>
  );
};

TeamMagma.propTypes = {};

export default TeamMagma;
