import React, { useState, useContext } from "react";
import PropTypes from "prop-types";

import {
  Container,
  Col,
  Row,
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  Spinner
} from "reactstrap";

import { AvForm, AvField } from "availity-reactstrap-validation";

import { StoreCard } from "../Store";

import { CollectionView, dao, functions } from "../../api/firebase";
import { NotificationCtx, AuthenticationCtx } from "../../contexts";
import { Routes } from "../../api";

const CreateStoreModal = props => {
  const { notify } = useContext(NotificationCtx);
  const [spinner, setSpinner] = useState(false);

  const create = (_, values) => {
    setSpinner(true);
    functions
      .createStore(values.name)
      .then(res => {
        if (res.data.status >= 400) {
          notify("danger", res.data.message);
        } else {
          notify("success", res.data.message);
          window.location.href = `${Routes.stores}/${res.data.store_id}`;
        }
      })
      .catch(err => notify("warning", `An err occurred ${err.message}`))
      .finally(() => {
        props.toggle();
        setSpinner(false);
      });
  };

  return (
    <Modal unmountOnClose={false} isOpen={props.isOpen} toggle={props.toggle}>
      <ModalHeader>Create A New Store</ModalHeader>
      <ModalBody>
        {spinner ? (
          <Spinner color="primary" />
        ) : (
          <AvForm onValidSubmit={create}>
            <AvField
              name="name"
              type="text"
              label="Store Name"
              validate={{
                minLength: { value: 3, errorMessage: "Name is too short" },
                maxLength: { value: 30, errorMessage: "Name is too long" }
              }}
            />
            <Button type="submit" color="primary">
              Create
            </Button>
          </AvForm>
        )}
      </ModalBody>
    </Modal>
  );
};

const storesView = new CollectionView(dao.stores);

const Stores = () => {
  const { user, toggleModal } = useContext(AuthenticationCtx);
  const [storeDocs, setStoreDocs] = useState(storesView.docs);
  const [createModalOpen, setCreateModalOpen] = useState(false);

  if (!storesView.started) storesView.start().then(setStoreDocs);

  const toggleCreateModal = () => setCreateModalOpen(!createModalOpen);

  return (
    <Container>
      <Row>
        <Col style={{ textAlign: "center" }}>
          <h1>Stores</h1>
        </Col>
      </Row>
      <Row style={{ margin: "1em auto" }}>
        <Col>
          <Button
            onClick={() => (user ? toggleCreateModal() : toggleModal())}
            color="primary"
          >
            Start a new Store
          </Button>
        </Col>
      </Row>
      <Row>
        {storeDocs.map(d => (
          <Col key={d.id} sm={12}>
            <StoreCard doc={d} />
          </Col>
        ))}
      </Row>
      <CreateStoreModal isOpen={createModalOpen} toggle={toggleCreateModal} />
    </Container>
  );
};

Stores.propTypes = {};

export default Stores;
