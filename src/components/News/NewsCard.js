import React from "react";
import PropTypes from "prop-types";

import { Collapse, Col, Row, Container, Card, CardBody } from "reactstrap";

const NewsDetails = props => {
  return (
    <Container fluid>
      <Collapse isOpen={props.isOpen}>{props.doc.data().content}</Collapse>
    </Container>
  );
};

const NewsCard = props => {
  const [isOpen, setIsOpen] = React.useState(false);

  const getStyle = () => {
    return {
      border: "1px solid grey",
      color: "white",
      backgroundColor: "transparent"
    };
  };

  return (
    <Col xs={isOpen ? 6 : 2}>
      <a onClick={() => setIsOpen(!isOpen)}>
        <Card style={getStyle()}>
          <CardBody
            style={{ padding: "0.8em", textAlign: isOpen ? "left" : "" }}
          >
            {props.doc.data().title}
          </CardBody>
          <NewsDetails isOpen={isOpen} doc={props.doc} />
        </Card>
      </a>
    </Col>
  );
};

NewsCard.propTypes = {};

export default NewsCard;
