import React from "react";
import PropTypes from "prop-types";

import { Container, Row, Col } from "reactstrap";
import { CollectionView, dao } from "../../api/firebase";
import NewsCard from "./NewsCard";

const newsView = new CollectionView(dao.news, 6);

const News = props => {
  const [newsDocs, setNewsDocs] = React.useState([]);

  const fifteenDaysAgo = new Date(new Date() - 15 * 24 * 60 * 60 * 1000);

  if (!newsView.started) {
    newsView.start(["createdAt", ">", fifteenDaysAgo]).then(docs => {
      let r = [];
      for (let i = 0; i < 30; i++) r = [...r, docs[0]];
      setNewsDocs(r);
    });
  }

  return (
    <Container
      fluid
      style={{
        overflowX: 'auto'
      }}
    >
      <Row>
        {newsDocs.map(d => (
          <NewsCard doc={d} key={d.id} />
        ))}
      </Row>
    </Container>
  );
};

News.propTypes = {};

export default News;
