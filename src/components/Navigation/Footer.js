import React, { useContext } from "react";

import { Container, Row, Col } from "reactstrap";

import { WindowSizeCtx } from "../../contexts";

const footerStyle = {
  backgroundColor: "#343A40",
  borderTop: "1px solid #E7E7E7",
  textAlign: "center",
  padding: "8px",
  position: "fixed",
  left: "0",
  bottom: "0",
  height: "40px",
  width: "100%"
};

const phantom = {
  display: "block",
  padding: "20px",
  height: "60px",
  width: "100%"
};

const itemStyle = {};

const FooterItem = props => {
  return (
    <a href={props.href} style={itemStyle} className="footer-item">
      {props.children}
    </a>
  );
};

const Footer = props => {
  const { windowSize } = useContext(WindowSizeCtx);
  return (
    <div>
      <div style={phantom} />
      <Container fluid style={footerStyle}>
        <Row>
          <Col>
            <FooterItem href="https://gitlab.com/JonoAugustine/teammagma">
              {windowSize === "sm" || windowSize === "xs"
                ? `Open Sourced by J.G. Augustine`
                : `Open Sourced by Jono Augustine`}
            </FooterItem>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Footer;
