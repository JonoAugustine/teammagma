import React, { useContext } from "react";
import PropTypes from "prop-types";

import { Col, Container, NavbarToggler } from "reactstrap";
import { Animated } from "react-animated-css";

import { WindowSizeCtx } from "../../contexts";

import "./navigation.css";

export const SidebarItem = props => {
  return (
    <li className="sidebar-item">
      <a href={props.href} onClick={() => console.log("CLICK")}>
        {props.children}
      </a>
    </li>
  );
};

SidebarItem.propTypes = {
  href: PropTypes.string
};

export const SidebarNav = props => {
  return <ul className="sidebar-nav">{props.children}</ul>;
};

export const Sidebar = props => {
  const { windowSize } = useContext(WindowSizeCtx);

  let style =
    windowSize === "lg" || windowSize === "xl"
      ? {
          position: "relative",
          left: "0",
          display: ""
        }
      : {
          zIndex: "9999",
          position: "absolute",
          right: "0",
          top: "4.5em",
          display: props.isOpen ? "" : "none"
        };

  return (
    <Col sm={12} md={6} lg={3} xl={2} style={style}>
      <Animated
        animationIn={"slideInLeft"}
        animationOut={"slideOutLeft"}
        isVisible={props.isOpen}
      >
        <Container
          style={{
            backgroundColor: "#1e1e1e",
            borderRadius: "10px",
            border: "1px solid red",
            padding: 0
          }}
        >
          <div className="row sidebar-wrapper-row">
            <Col className="sidebar">{props.children}</Col>
            {windowSize !== "lg" && windowSize !== "xl" ? (
              <NavbarToggler
                onClick={props.toggle}
                style={{
                  backgroundColor: "#6c757d",
                  color: "red",
                  height: "3em",
                  margin: "0.5em 1em"
                }}
              />
            ) : (
              ""
            )}
          </div>
        </Container>
      </Animated>
    </Col>
  );
};

Sidebar.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func
};
