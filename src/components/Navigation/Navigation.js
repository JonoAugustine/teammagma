import React, { useContext } from "react";

import {
  Collapse,
  Col,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

import { AuthenticationCtx } from "../../contexts";

import { Routes } from "../../api";

import "./navigation.css";

import { dao } from "../../api/firebase";

import MagmaLogo from "../../images/magma_logo.png";

const AuthNav = props => {
  return (
    <UncontrolledDropdown nav inNavbar>
      <DropdownToggle nav>
        {props.displayName ? props.displayName : "Account"}
      </DropdownToggle>
      <DropdownMenu right>
        <DropdownItem>
          <NavLink href={Routes.profile}>Profile</NavLink>
        </DropdownItem>
        {props.isAdmin ? (
          <DropdownItem>
            <NavLink href={Routes.admin}>Admin</NavLink>
          </DropdownItem>
        ) : (
          ""
        )}
        <DropdownItem divider />
        <DropdownItem>
          <NavLink onClick={() => dao.userLogout()}>Logout</NavLink>
        </DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};

const NonAuthNav = () => {
  const { toggleModal } = useContext(AuthenticationCtx);

  return (
    <NavItem>
      <NavLink onClick={toggleModal}>Signup</NavLink>
    </NavItem>
  );
};

export const Navigation = props => {
  const [isOpen, setIsOpen] = React.useState(false);

  const toggle = () => setIsOpen(!isOpen);

  const { user, isAdmin } = useContext(AuthenticationCtx);

  return (
    <Navbar color="dark" dark expand="md" sticky="top">
      <Col style={{ paddingRight: "0" }}>
        <NavbarBrand href={Routes.home}>
          <img src={MagmaLogo} alt="Team Magma" style={{ maxHeight: "35px" }} />
        </NavbarBrand>
      </Col>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar>
        <Nav>
          <NavItem>
            <NavLink href={Routes.home}>Listings</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href={Routes.stores}>Stores</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href={Routes.about}>Join Magma</NavLink>
          </NavItem>
          {user ? (
            <AuthNav displayName={user.displayName} isAdmin={isAdmin} />
          ) : (
            <NonAuthNav />
          )}
        </Nav>
      </Collapse>
    </Navbar>
  );
};
