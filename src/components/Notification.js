import React, { useState } from "react";

import { Alert } from "reactstrap";

const Notification = props => {
  const [isOpen, setOpen] = useState(true);

  const dismiss = () => setOpen(false);

  if (typeof props.timeout == 'number') {
    setTimeout(() => dismiss(), props.timeout)
  }

  return (
    <Alert color={props.color} isOpen={isOpen} toggle={dismiss}>
      {props.children}
    </Alert>
  );
};

export default Notification;
