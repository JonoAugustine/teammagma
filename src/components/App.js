import React, { useState } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import { Container } from "reactstrap";

import uuid from "uuid/v1";

import { Navigation, Footer } from "./Navigation";
import Notification from "./Notification";

import {
  NotificationCtx,
  AuthenticationCtxProvider,
  WindowSizeCtxProvider
} from "../contexts";
import { Home, Profile, TeamMagma, Stores } from "./Pages";
import { Store } from "./Store";

import { Routes } from "../api";

import { messaging } from "../api/firebase";

import "../style/App.css";

const App = () => {
  const [alerts, setAlerts] = useState([]);

  const notify = (color, children) => {
    setAlerts([
      ...alerts,
      <Notification key={uuid()} color={color} timeout={10000}>
        {children}
      </Notification>
    ]);
  };

  messaging.onMessage(payload => {
    notify(payload.color ? payload.color : "primary", payload.message);
  });

  return (
    <WindowSizeCtxProvider>
      <NotificationCtx.Provider value={{ notify: notify }}>
        <AuthenticationCtxProvider>
          <div id="app">
            <Navigation />
            <Router>
              <Route exact path={Routes.home} component={Home} />
              <Route exact path={Routes.stores}>
                <Stores />
              </Route>
              <Route path={Routes.storePage} component={Store} />
              <Route path={Routes.about} component={TeamMagma} />
              <Route path={Routes.profile} component={Profile} />
            </Router>
            <Footer />
          </div>
          <Container id="notifications">{alerts}</Container>
        </AuthenticationCtxProvider>
      </NotificationCtx.Provider>
    </WindowSizeCtxProvider>
  );
};

export default App;
