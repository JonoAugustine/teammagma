import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";
import { BrowserRouter as Router, Route } from "react-router-dom";

import { dao, CollectionView } from "../../api/firebase";

import { Container, Col, Row } from "reactstrap";
import {
  AuthenticationCtx,
  NotificationCtx,
  WindowSizeCtx
} from "../../contexts";
import { Sidebar, SidebarNav, SidebarItem } from "../Navigation";
import { Routes } from "../../api";
import { StoreHome } from "./StoreHome";
import StoreSettings from "./StoreSettings";

import { firestore } from "firebase/app";

const StoreSidePanel = props => {
  const { windowSize } = useContext(WindowSizeCtx);
  const [sidebarOpen, setSidebarOpen] = useState(
    windowSize === "lg" || windowSize === "xl"
  );

  // Hide & show on window size change
  useEffect(() => setSidebarOpen(windowSize === "xl" || windowSize === "lg"), [
    windowSize
  ]);

  const toggleSidebar = () => setSidebarOpen(!sidebarOpen);

  return (
    <Sidebar isOpen={sidebarOpen} toggle={toggleSidebar}>
      <SidebarNav>
        <SidebarItem href={`${Routes.stores}/${props.doc.id}`}>
          Store
        </SidebarItem>
        <SidebarItem href={Routes.storeSettings(props.doc.id)}>
          Settings
        </SidebarItem>
      </SidebarNav>
    </Sidebar>
  );
};

const Store = props => {
  const store_id = props.match.params.store_id;

  const { user } = useContext(AuthenticationCtx);
  const { notify } = useContext(NotificationCtx);
  const [storeDoc, setStoreDoc] = useState(null);
  const [isOwner, setIsOwner] = useState(false);
  const [isSeller, setIsSeller] = useState(false);

  useEffect(() => {
    if (user !== null && storeDoc !== null) {
      if (storeDoc.data().owner.uid === user.uid) {
        setIsOwner(true);
        setIsSeller(true);
      } else {
        setIsOwner(false);
        setIsSeller(storeDoc.data().sellers.includes(user.uid));
      }
    }
  }, [storeDoc]);

  if (storeDoc === null) {
    dao.stores
      .doc(store_id)
      .get()
      .then(setStoreDoc)
      .catch(err => {});
  }

  if (storeDoc === null) return <div></div>;

  return (
    <Container fluid>
      <Row>
        {isOwner ? <StoreSidePanel doc={storeDoc} /> : ""}
        <Col>
          <Router>
            <Route exact path={Routes.storePage}>
              <StoreHome
                id={store_id}
                storeData={storeDoc.data()}
                isOwner={isOwner}
                isSeller={isSeller}
                listingView={
                  new CollectionView(dao.listings, 15, [
                    firestore.FieldPath.documentId(),
                    "in",
                    [...storeDoc.data().listings, "x"]
                  ])
                }
              />
            </Route>
            <Route exact path={Routes.storeSettings(store_id)}>
              <StoreSettings id={store_id} doc={storeDoc} isOwner={isOwner} />
            </Route>
          </Router>
        </Col>
      </Row>
    </Container>
  );
};

Store.propTypes = {};

export default Store;
