import React from "react";
import PropTypes from "prop-types";

import { Col, Row, Card } from "reactstrap";

import "./store.css";
import { Routes } from "../../api";

const StoreCard = props => {

  const numListings = props.doc.data().listings.length;
  const numSellers = props.doc.data().sellers.length + 1;

  return (
    <Card
      onClick={() =>
        (window.location.href = `${Routes.stores}/${props.doc.id}`)
      }
      style={{
        cursor: "pointer",
        padding: "0.5em",
        margin: "0.3em",
        border: "none",
        borderRadius: "0",
        borderBottom: "1px solid",
        borderColor: props.doc.data().color ? props.doc.data().color : "red",
        backgroundColor: "transparent"
      }}
    >
      <Row>
        <Col
          style={{
            maxHeight: "1.6em",
            overflow: "hidden"
          }}
        >
          {props.doc.data().name}
        </Col>
        <Col xs={4}>
          {numListings} listing{numListings === 1 ? "" : "s"}
        </Col>
        <Col className="d-none d-sm-block">
          {numSellers} seller{numSellers === 1 ? "" : "s"}
        </Col>
      </Row>
    </Card>
  );
};

StoreCard.propTypes = {
  doc: PropTypes.object.isRequired
};

export default StoreCard;
