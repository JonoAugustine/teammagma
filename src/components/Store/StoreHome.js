import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";

import {
  Container,
  Col,
  Row,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";

import { ListingCreation } from "../Listing";

import { ListingSection } from "../Listing";

import { NotificationCtx } from "../../contexts";

import { dao, CollectionView } from "../../api/firebase";

const AddListingModal = props => {
  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle}>
      <ModalHeader>Add Listing To {props.name}</ModalHeader>
      <ModalBody>
        <ListingCreation />
      </ModalBody>
      <ModalFooter></ModalFooter>
    </Modal>
  );
};

const StoreHome = props => {
  const store_id = props.id;

  const { notify } = useContext(NotificationCtx);
  const [storeName, setStoreName] = useState("");
  const [addModal, setAddModal] = useState(false);

  const toggleAddModal = () => setAddModal(!addModal);

  useEffect(() => {
    setStoreName(props.storeData ? props.storeData.name : "");
  }, []);

  if (props.storeData === null) return <div></div>;

  return (
    <Container>
      <Row>
        <Col style={{ textAlign: "center" }}>
          <Row>
            <Col>
              <input
                id="store_name"
                style={{
                  textAlign: "center",
                  width: "100%",
                  fontSize: "2em",
                  border: "none",
                  color: "white",
                  backgroundColor: "transparent"
                }}
                readOnly={props.isOwner}
                value={storeName}
                name="name"
                type="text"
                onChange={e => {
                  setStoreName(e.target.value);
                }}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                color="primary"
                style={{
                  display: storeName === props.storeData.name ? "none" : ""
                }}
                onClick={() => {
                  const nName = document.getElementById("store_name").value;
                  dao
                    .updateStore(store_id, { name: nName })
                    .then(() => notify("success", "Store name updated"))
                    .catch(err =>
                      notify("warning", `An err occurred ${err.message}`)
                    );
                }}
              >
                Save
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
      <ListingSection
        noImage={props.isOwner}
        listingsView={props.listingView}
      />
      {props.isSeller ? (
        <Row style={{ textAlign: "right" }}>
          <Col>
            <Button onClick={toggleAddModal} color="primary">
              Add listing
            </Button>
          </Col>
        </Row>
      ) : (
        ""
      )}
      <AddListingModal
        toggle={toggleAddModal}
        isOpen={addModal}
        name={props.storeData.name}
        id={props.id}
      />
    </Container>
  );
};

StoreHome.propTypes = {
  storeData: PropTypes.object.isRequired,
  listingView: PropTypes.instanceOf(CollectionView).isRequired
};

export { StoreHome };
