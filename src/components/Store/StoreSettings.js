import React, { useState, useContext } from "react";
import PropTypes from "prop-types";

import {
  Container,
  Col,
  Row,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";

import { AvForm, AvField } from "availity-reactstrap-validation";
import { dao } from "../../api/firebase";
import { NotificationCtx } from "../../contexts";

const DeleteModal = props => {
  const { notify } = useContext(NotificationCtx);

  const deleteStore = () => {
    dao
      .deleteStore(props.doc.id)
      .then(() => {
        props.toggle();
        notify("success", "Store deleted");
        setTimeout(() => (window.location.href = "/stores"), 1000);
      })
      .catch(err => notify("warning", `An err occurred. ${err.message}`))
      .finally(() => props.toggle());
  };

  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle}>
      <ModalHeader>Delete Store {props.doc.data().name}?</ModalHeader>
      <ModalBody>
        <AvForm onValidSubmit={deleteStore}>
          <AvField
            name="name"
            label="Enter the Store Name to confirm"
            type="text"
            validate={{
              pattern: { value: `^${props.doc.data().name}$` }
            }}
          />
          <Button outline color="danger" type="submit">
            Delete Store
          </Button>
        </AvForm>
      </ModalBody>
    </Modal>
  );
};

const StoreSettings = props => {
  const [deleteModal, setDeleteModal] = useState(false);

  const update = (_, values) => {
    // Todo
  };

  return (
    <Container>
      <Row>
        <Col>
          <AvForm
            onValidSubmit={update}
            model={{
              name: props.doc.data().name
            }}
          >
            <AvField name="name" label="Store Name" type="text" />
            <Button type="submit" color="primary">
              Save Changes
            </Button>
          </AvForm>
          <Button
            onClick={() => setDeleteModal(true)}
            color="danger"
            outline
            style={{ bottom: 0, left: 0, margin: "50% auto 0 auto" }}
          >
            Delete Store
            <DeleteModal
              doc={props.doc}
              isOpen={deleteModal}
              toggle={() => setDeleteModal(!deleteModal)}
            />
          </Button>
        </Col>
      </Row>
    </Container>
  );
};

StoreSettings.propTypes = {};

export default StoreSettings;
