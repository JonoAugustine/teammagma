import React, { useState } from "react";

import { Card, CardImg, CardHeader, Collapse } from "reactstrap";

import { PokeApi } from "../../api";

import "./listing.css";

import TEMP_IMG from "../../images/psyduck_purple.png";

/**
 * Collapsable listing card
 */
const ListingCard = props => {
  const [isOpen, setIsOpen] = useState(false);
  const [image, setImage] = useState(TEMP_IMG);

  if (props.data.name !== null) {
    props.data.details
      ? PokeApi.getPokemonByName(props.data.name).then(poke => {
          setImage(poke.sprites.front_default);
        })
      : PokeApi.getItemByName(props.data.name).then(item => {
          setImage(item.sprites.default);
        });
  }

  return (
    <a onClick={() => setIsOpen(!isOpen)} style={{ cursor: "pointer" }}>
      <Card body style={style.card}>
        <CardHeader style={style.cardHeader}>
          <p style={{ float: "left" }}>{props.data.name}</p>
          <p style={{ float: "right" }}>
            {(props.data.price / 1000)
              .toString()
              .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")}
            K
          </p>
        </CardHeader>
        <Collapse
          isOpen={!isOpen}
          style={{ textAlign: "center", alignContent: "center" }}
        >
          {props.noImage ? (
            ""
          ) : (
            <CardImg
              src={image}
              top
              style={{ maxWidth: "150px", maxHeight: "150px" }}
            />
          )}
        </Collapse>
        <Collapse isOpen={isOpen}>{props.children}</Collapse>
      </Card>
    </a>
  );
};

const style = {
  card: {
    backgroundColor: "transparent",
    border: "1px solid steelgrey"
  },
  cardHeader: {
    padding: 0,
    backgroundColor: "Transparent",
    fontSize: "1.2em"
  }
};

export default ListingCard;
