import React, { useContext, useState, useEffect } from "react";

import { Container, Row, Col, Button } from "reactstrap";
import {
  AvForm,
  AvField,
  AvRadioGroup,
  AvRadio,
  AvGroup
} from "availity-reactstrap-validation";

import { ListingSpec } from ".";
import ProtoListingCard from "./ProtoListingCard";

import { NotificationCtx, AuthenticationCtx } from "../../contexts";

import "./listing.css";
import { PokeApi } from "../../api";
import { dao } from "../../api/firebase";

const ItemListingCreation = props => {
  return <div>TODO</div>;
};

const ivRange = {
  min: { value: 0 },
  max: { value: 32 }
};

const maxEv = 510;

const PokeListingCreation = props => {
  const { notify } = useContext(NotificationCtx);
  const { user } = useContext(AuthenticationCtx);

  const regionList = ["", "kanto", "johto", "hoenn", "sinnoh"];
  const [pokeList, setPokeList] = useState([]);
  const [natures, setNatures] = useState([]);
  const [abilities, setAbilities] = useState([]);
  const [hiddenPowers, sethiddenPowers] = useState([]);

  const [price, setPrice] = useState(0);
  const [searchFilter, setSearchFilter] = useState("");

  const [name, setName] = useState(null);
  const [evRem, setEvRem] = useState(maxEv);

  const [details, setDetails] = useState(ListingSpec.DetailTemplate.mon);

  useEffect(() => {
    const s = document.getElementById("select").value;
    if (s) setName(s);
  }, [searchFilter]);

  useEffect(() => {
    if (name !== null) {
      PokeApi.getPokemonByName(name)
        .then(r => setAbilities(["", ...r.abilities.map(a => a.ability.name)]))
        .then(() => setDetails({ ...details, ability: "" }))
        .then(() => PokeApi.getTypesList())
        .then(r => sethiddenPowers(["", ...r.results.map(t => t.name)]))
        .then(() => setDetails({ ...details, hidden_power: "" }))
        .catch(err => console.log(err));
    }
  }, [name]);

  // Track EVs
  useEffect(() => {
    let sum = Object.values(details.evs).reduce((acc, cur) => acc + cur);
    setEvRem(maxEv - sum);
  }, [details]);

  if (user === null) return <div></div>;

  if (natures.length === 0) {
    PokeApi.getNaturesList()
      .then(resp => setNatures(["", ...resp.results.map(n => n.name)]))
      .then(() => setDetails({ ...details, nature: "" }))
      .catch(err => console.log(err));
  }

  if (pokeList.length === 0) {
    PokeApi.getPokemonsList()
      .then(response => {
        setPokeList(response.results);
        setName(response.results[0].name);
      })
      .catch(err => console.log(err));
  }

  const IEVRow = props_ => (
    <Row
      style={{
        margin: 0,
        height: "40px",
        fontSize: "1.5em",
        ...props.style
      }}
    >
      {props_.children}
    </Row>
  );

  const createPokeListing = (_, values) => {
    if (evRem < 0) {
      return notify(
        "danger",
        <Row className="justify-content-center">Too many EVs!</Row>
      );
    }
    dao
      .createListing(name, price, details)
      .then(docRef => {
        notify("success", <React.Fragment>Listing Posted!</React.Fragment>);
      })
      .catch(e => {
        console.log(e);
        notify(
          "danger",
          <React.Fragment>
            An err occurred. Failed to create listing due to "{e.message}"
          </React.Fragment>
        );
      });
  };

  return (
    <Row>
      <Col
        xs={12}
        lg={6}
        style={{ marginBottom: "2em" }}
        className="listing-creation"
      >
        <AvForm onValidSubmit={createPokeListing} model={{ sex: "Female" }}>
          <label>
            <h3>Pokemon</h3>
          </label>
          <AvGroup>
            <AvField
              type="text"
              name="search"
              label="Search"
              onChange={e => {
                setSearchFilter(e.target.value);
              }}
            />
            <AvField
              id="select"
              type="select"
              name="select"
              required
              onChange={e => setName(e.target.value)}
            >
              {pokeList
                .filter(p => p.name.includes(searchFilter))
                .map(p => (
                  <option key={p.name}>{p.name}</option>
                ))}
            </AvField>
          </AvGroup>
          <AvField
            type="number"
            number="true"
            name="price"
            label={`Price: ${price
              .toString()
              .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")}`}
            required
            onChange={e => setPrice(e.target.value)}
          />
          <Row>
            <Col>
              <Row>
                <Col></Col>
                <Col>IV</Col>
                <Col>EV</Col>
              </Row>
              <Row>
                <Col>
                  <IEVRow>ATK</IEVRow>
                  <IEVRow>DEF</IEVRow>
                  <IEVRow>SPD</IEVRow>
                  <IEVRow>SPATK</IEVRow>
                  <IEVRow>SPDEF</IEVRow>
                  <IEVRow>HP</IEVRow>
                </Col>
                <Col>
                  {["atk", "def", "spd", "spatk", "spdef", "hp"].map(n => (
                    <AvField
                      type="number"
                      style={{ height: "1.6em" }}
                      validate={ivRange}
                      name={`iv_${n}`}
                      key={n}
                      onChange={e => {
                        const ivs = {
                          ...details.ivs
                        };
                        ivs[n] = isNaN(e.target.value)
                          ? 0
                          : parseInt(e.target.value);
                        setDetails({
                          ...details,
                          ivs: ivs
                        });
                        console.log(details);
                      }}
                    />
                  ))}
                </Col>
                <Col>
                  {["atk", "def", "spd", "spatk", "spdef", "hp"].map(n => (
                    <AvField
                      type="number"
                      style={{ height: "1.6em" }}
                      key={n}
                      validate={{
                        min: { value: 0 },
                        max: { value: 252 }
                      }}
                      name={`ev_${n}`}
                      onChange={e => {
                        const evs = {
                          ...details.evs
                        };
                        evs[n] = isNaN(e.target.value)
                          ? 0
                          : parseInt(e.target.value);
                        setDetails({
                          ...details,
                          evs: evs
                        });
                        console.log(details);
                      }}
                    />
                  ))}
                </Col>
              </Row>
              <Row>
                <Col sm={4}>
                  <AvField
                    name="nature"
                    label="Nature"
                    type="select"
                    onChange={e => {
                      setDetails({
                        ...details,
                        nature: e.target.value
                      });
                    }}
                  >
                    {natures.map(n => (
                      <option key={n}>{n}</option>
                    ))}
                  </AvField>
                </Col>
                <Col sm={4}>
                  <AvField
                    name="ability"
                    label="Ability"
                    type="select"
                    onChange={e => {
                      setDetails({
                        ...details,
                        ability: e.target.value
                      });
                    }}
                  >
                    {abilities.map(n => (
                      <option key={n}>{n}</option>
                    ))}
                  </AvField>
                </Col>
                <Col sm={4}>
                  <AvField
                    name="hidden_power"
                    label="Hidden Power"
                    type="select"
                    onChange={e => {
                      setDetails({
                        ...details,
                        hidden_power: e.target.value
                      });
                    }}
                  >
                    {hiddenPowers.map(n => (
                      <option key={n}>{n}</option>
                    ))}
                  </AvField>
                </Col>
                <Col sm={4}>
                  <AvField
                    name="region"
                    label="Region"
                    type="select"
                    onChange={e => {
                      setDetails({
                        ...details,
                        region: e.target.value
                      });
                    }}
                  >
                    {regionList.map(n => (
                      <option key={n}>{n}</option>
                    ))}
                  </AvField>
                </Col>
                <Col sm={4}>
                  <h6 style={{ margin: "0 0 10px 0" }}>Sex</h6>
                  <AvRadioGroup inline name="sex" required>
                    <AvRadio
                      label="Male"
                      value="Male"
                      onChange={() =>
                        setDetails({
                          ...details,
                          sex: false
                        })
                      }
                    />
                    <AvRadio
                      label="Female"
                      value="Female"
                      onChange={() =>
                        setDetails({
                          ...details,
                          sex: true
                        })
                      }
                    />
                  </AvRadioGroup>
                </Col>
                <Col sm={4}>
                  <AvField
                    name="ot"
                    type="text"
                    label="Original Trainer"
                    required
                    onChange={e => {
                      // TODO OT validation
                      setDetails({
                        ...details,
                        ot: e.target.value
                      });
                    }}
                  />
                </Col>
              </Row>
            </Col>
          </Row>

          <Row className="justify-content-center">
            <Button style={{ marginTop: "1em" }} color="success">
              Create Listing
            </Button>
          </Row>
        </AvForm>
      </Col>
      <ProtoListingCard
        name={name}
        price={price}
        seller={user.displayName}
        type={ListingSpec.Type.mon}
        details={details}
      />
    </Row>
  );
};

const ListingCreation = props => {
  const [listingType, setListingType] = useState(ListingSpec.Type.mon);

  return (
    <Container fluid>
      <Row>
        <Col>
          <AvForm model={{ type: "Pokemon" }}>
            <AvRadioGroup name="type" label="Listing Type" required inline>
              <AvRadio
                label="Pokemon"
                value="Pokemon"
                onChange={() => setListingType(ListingSpec.Type.mon)}
              />
              <AvRadio
                label="Item"
                value="Item"
                onChange={() => setListingType(ListingSpec.Type.item)}
              />
            </AvRadioGroup>
          </AvForm>
          {listingType === ListingSpec.Type.mon ? (
            <PokeListingCreation />
          ) : (
            <ItemListingCreation />
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default ListingCreation;
