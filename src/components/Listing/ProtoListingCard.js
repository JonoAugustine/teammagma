import React, { useState } from "react";

import {
  Container,
  Col,
  Row,
  Card,
  CardImg,
  CardHeader,
  CardFooter,
  Collapse,
  Button
} from "reactstrap";

import { PokeApi } from "../../api";

import { ListingSpec } from "./index";
import "./listing.css";

import TEMP_IMG from "../../images/psyduck_purple.png";

const DetailsCol = props => {
  return (
    <Col
      sm={12}
      md={3}
      lg={4}
      style={{
        padding: "0 0 10px 0",
        marginBottom: "0.5em",

        borderBottom: "1px dotted red"
      }}
    >
      <h5>{props.header}</h5>
      {props.children}
    </Col>
  );
};

const Trait = props => {
  return (
    <Row style={{ margin: "0", padding: "0" }}>
      {props.name}: {props.value}
    </Row>
  );
};

const formatDetailsITEM = data => {
  // TODO
  return <div>TODO</div>;
};

const formatDetailsMON = data => {
  return (
    <Row>
      <DetailsCol header="IVs">
        <Trait
          name="  ATK"
          value={data.details && data.details.ivs ? data.details.ivs.atk : 0}
        />
        <Trait
          name="  DEF"
          value={data.details && data.details.ivs ? data.details.ivs.def : 0}
        />
        <Trait
          name="  SPD"
          value={data.details && data.details.ivs ? data.details.ivs.spd : 0}
        />
        <Trait
          name="SPATK"
          value={data.details && data.details.ivs ? data.details.ivs.spatk : 0}
        />
        <Trait
          name="SPDEF"
          value={data.details && data.details.ivs ? data.details.ivs.spdef : 0}
        />
        <Trait
          name="   HP"
          value={data.details && data.details.ivs ? data.details.ivs.hp : 0}
        />
      </DetailsCol>
      <DetailsCol header="EVs">
        <Trait
          name="  ATK"
          value={data.details && data.details.evs ? data.details.evs.atk : 0}
        />
        <Trait
          name="  DEF"
          value={data.details && data.details.evs ? data.details.evs.def : 0}
        />
        <Trait
          name="  SPD"
          value={data.details && data.details.evs ? data.details.evs.spd : 0}
        />
        <Trait
          name="SPATK"
          value={data.details && data.details.evs ? data.details.evs.spatk : 0}
        />
        <Trait
          name="SPDEF"
          value={data.details && data.details.evs ? data.details.evs.spdef : 0}
        />
        <Trait
          name="   HP"
          value={data.details && data.details.evs ? data.details.evs.hp : 0}
        />
      </DetailsCol>
      <DetailsCol header="Traits">
        <Trait
          name="  Nature"
          value={data.details ? data.details.nature : "unset"}
        />
        <Trait
          name=" Ability"
          value={data.details ? data.details.ability : "unset"}
        />
        <Trait
          name="H. Power"
          value={data.details ? data.details.hidden_power : "unset"}
        />
        <Trait
          name="  Region"
          value={data.details ? data.details.region : "unset"}
        />
        <Trait
          name="     Sex"
          value={
            data.details ? (data.details.sex ? "Female" : "Male") : "unset"
          }
        />
      </DetailsCol>
    </Row>
  );
};

const formatDetails = data => {
  switch (data.type) {
    case ListingSpec.Type.item:
      return formatDetailsITEM(data);
    case ListingSpec.Type.mon:
      return formatDetailsMON(data);
    default:
      return <div>An err occurred</div>;
  }
};

/**
 * Collapsable listing card
 */
const ProtoListingCard = props => {
  const [isOpen, setIsOpen] = useState(false);
  const [image, setImage] = useState(TEMP_IMG);

  if (props.name !== null) {
    try {
      if (props.type === ListingSpec.Type.mon) {
        PokeApi.getPokemonByName(props.name).then(poke => {
          setImage(poke.sprites.front_default);
        });
      } else if (props.type === ListingSpec.Type.item) {
        PokeApi.getItemByName(props.name).then(item => {
          setImage(item.sprites.default);
        });
      }
    } catch (e) {}
  }

  return (
    <Col style={{ paddingBottom: ".5em" }}>
      <Card
        body
        style={{ backgroundColor: "#292929", border: "1px solid red" }}
      >
        <CardHeader>
          <h4 style={{ float: "left" }}>{props.name}</h4>
          <h4 style={{ float: "right" }}>
            {(props.price / 1000)
              .toString()
              .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")}
            K
          </h4>
        </CardHeader>
        <CardImg src={image} top />
        <CardFooter>
          <Collapse isOpen={true}>
            <Container fluid style={{ marginTop: "0.5em" }}>
              {formatDetails(props)}
            </Container>
          </Collapse>
        </CardFooter>
      </Card>
    </Col>
  );
};

export default ProtoListingCard;
