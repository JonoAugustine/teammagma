import React, { useState } from "react";
import PropTypes from "prop-types";

import { Row, Col, Button } from "reactstrap";

import { ListingCard, PokeListingDetails, ItemListingDetails } from ".";

import SendOfferModal from "./SendOfferModal";
import { ReportModal } from "../ReportModal";

import { CollectionView } from "../../api/firebase";

const ListingSection = props => {
  const [docs, setDocs] = useState([]);
  const [reportModalOpen, setReportModalOpen] = useState(false);
  const [offerModalOpen, setOfferModalOpen] = useState(false);
  const [modalData, setModalData] = useState(null);

  const toggleOfferModal = () => setOfferModalOpen(!offerModalOpen);
  const toggleReportModal = () => setReportModalOpen(!reportModalOpen);

  if (props.listingsView === null) return <div></div>;

  if (!props.listingsView.started) {
    props.listingsView.start(["status.offer_id", "==", null]).then(setDocs);
  }

  return (
    <Row
      style={{
        paddingTop: "1em",
        overflowY: "auto",
        scrollbarWidth: "none"
      }}
    >
      {docs.map(doc => {
        const data = doc.data();
        const detailsSection =
          typeof data["details"] === "object" ? (
            <PokeListingDetails details={data.details} />
          ) : (
            <ItemListingDetails data={data} />
          );
        return (
          <Col key={doc.id} xs={12} md={4} lg={3} xl={3}>
            <ListingCard noImage={props.noImage} data={data}>
              {detailsSection}
              <Button
                color="primary"
                style={{ float: "right", marginBottom: "0.5em" }}
                onClick={() => {
                  setModalData({
                    lid: doc.id,
                    suid: data.uid,
                    name: data.name,
                    ask: data.price
                  });
                  toggleOfferModal();
                }}
              >
                Send Offer
              </Button>
              <Button
                color="danger"
                outline
                style={{ float: "right" }}
                onClick={() => {
                  setModalData({
                    header: `${data.name} (${doc.id})`,
                    options: ["vulgar name", "trolling"]
                  });
                  toggleReportModal();
                }}
              >
                Report Listing
              </Button>
            </ListingCard>
          </Col>
        );
      })}
      <SendOfferModal
        toggle={toggleOfferModal}
        isOpen={offerModalOpen}
        data={modalData}
      />
      <ReportModal
        toggle={toggleReportModal}
        isOpen={reportModalOpen}
        data={modalData}
      />
    </Row>
  );
};

ListingSection.propTypes = {
  listingsView: PropTypes.instanceOf(CollectionView).isRequired,
  noImage: PropTypes.bool
};

export default ListingSection;
