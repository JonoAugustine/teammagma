import ListingCard from "./ListingCard";
import ListingSection from "./ListingSection";
import ListingCreation from "./ListingCreation";
import PokeListingDetails from "./PokeListingDetails";
import ItemListingDetails from "./ItemListingDetails";

export {
  ListingSection,
  ListingCreation,
  ListingCard,
  PokeListingDetails,
  ItemListingDetails
};

export const ListingSpec = {
  Type: {
    mon: "mon",
    item: "item"
    // TODO more types
  },
  DetailTemplate: {
    mon: () => ({
      region: null,
      ot: null,
      nature: null,
      hidden_power: null,
      sex: true,
      ivs: {
        atk: 0,
        def: 0,
        spd: 0,
        spatk: 0,
        spdef: 0,
        hp: 0
      },
      evs: {
        atk: 0,
        def: 0,
        spd: 0,
        spatk: 0,
        spdef: 0,
        hp: 0
      }
    })
  }
};
