import React, { useState, useContext } from "react";
import PropTypes from "prop-types";

import { Button, Modal, ModalBody, ModalHeader } from "reactstrap";

import { AvForm, AvField } from "availity-reactstrap-validation";

import "./listing.css";

import { NotificationCtx } from "../../contexts";
import { dao } from "../../api/firebase";

// TODO offer another listing
const SendOfferModal = props => {
  const { notify } = useContext(NotificationCtx);
  const [priceFormated, setPriceFormated] = useState("");

  if (props.data === null || typeof props.data["ask"] === "undefined")
    return <div></div>;

  const sendOffer = (_, values) => {
    dao
      .createOffer(
        props.data.lid,
        props.data.suid,
        props.data.name,
        props.data.ask,
        parseInt(values.price)
      )
      .then(() => {
        props.toggle();
        notify("success", <React.Fragment>Offer Sent</React.Fragment>);
      })
      .catch(err => {
        console.log(err);
        props.toggle();
        notify(
          "danger",
          <React.Fragment>An err occurred. Please try again</React.Fragment>
        );
      });
  };
  
  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle}>
      <ModalHeader>Send Offer for {props.data.name}</ModalHeader>
      <ModalBody>
        <AvForm
          onValidSubmit={sendOffer}
          model={{ price: props.data.ask.toString() }}
        >
          <AvField
            stlye={{ color: "black" }}
            name="price"
            label="Offer"
            type="number"
            required
            helpMessage={priceFormated}
            onChange={e =>
              setPriceFormated(
                e.target.value.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
              )
            }
          />
          <Button color="success">Send</Button>
        </AvForm>
      </ModalBody>
    </Modal>
  );
};

SendOfferModal.propTypes = {
  data: PropTypes.object,
  isOpen: PropTypes.bool,
  toggle: PropTypes.func
};

export default SendOfferModal;
