import React, { useState, useContext } from "react";

import { Container, Col, Row } from "reactstrap";

import { PokeApi, statShorthand } from "../../api";

import "./listing.css";

import { NotificationCtx, AuthenticationCtx } from "../../contexts";
import { dao } from "../../api/firebase";

const DetailsCol = props => {
  return (
    <Col
      sm={12}
      md={props.lg ? props.lg : 6}
      style={{
        padding: "0 0 10px 0",
        marginBottom: "0.5em",
        textAlign: "center",
        borderBottom: "1px dotted red"
      }}
    >
      <h5>{props.header}</h5>
      {props.children}
    </Col>
  );
};

const Trait = props => {
  return (
    <Row style={{ margin: "0", padding: "0" }}>
      {props.name}: {props.value}
    </Row>
  );
};

const PokeListingDetails = props => {
  const [natureDetail, setNatureDetail] = useState(null);

  if (natureDetail === null) {
    PokeApi.getNatureByName(props.details.nature).then(resp => {
      if (typeof resp.increased_stat.name !== "string") return;
      setNatureDetail(
        `${props.details.nature}
         (-${statShorthand(resp.decreased_stat.name)}
         +${statShorthand(resp.increased_stat.name)})`
      );
    });
  }

  return (
    <Row>
      <DetailsCol header="IVs">
        <Trait name="  ATK" value={props.details.ivs.atk} />
        <Trait name="  DEF" value={props.details.ivs.def} />
        <Trait name="  SPD" value={props.details.ivs.spd} />
        <Trait name="SPATK" value={props.details.ivs.spatk} />
        <Trait name="SPDEF" value={props.details.ivs.spdef} />
        <Trait name="   HP" value={props.details.ivs.hp} />
      </DetailsCol>
      <DetailsCol header="EVs">
        <Trait name="  ATK" value={props.details.evs.atk} />
        <Trait name="  DEF" value={props.details.evs.def} />
        <Trait name="  SPD" value={props.details.evs.spd} />
        <Trait name="SPATK" value={props.details.evs.spatk} />
        <Trait name="SPDEF" value={props.details.evs.spdef} />
        <Trait name="   HP" value={props.details.evs.hp} />
      </DetailsCol>
      <DetailsCol header="Traits" lg={12}>
        <Trait
          name="  Nature"
          value={natureDetail ? natureDetail : props.details.nature}
        />
        <Trait name=" Ability" value={props.details.ability} />
        <Trait name="H. Power" value={props.details.hidden_power} />
        <Trait name="  Region" value={props.details.region} />
        <Trait name="     Sex" value={props.details.sex ? "Female" : "Male"} />
      </DetailsCol>
    </Row>
  );
};

export default PokeListingDetails;
