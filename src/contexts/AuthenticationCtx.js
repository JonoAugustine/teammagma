import React, { useState } from "react";

import { auth, messaging } from "../api/firebase";

import { AuthModal } from "../components/AuthModal";

export const AuthenticationCtx = React.createContext(null);

const AuthenticationCtxProvider = props => {
  const [user, setUser] = useState(props.user);
  const [isAdmin, setIsAdmin] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [messagingEnabled, setMessagingEnabled] = useState(0);

  const toggleModal = () => setModalOpen(!modalOpen);

  auth.onAuthStateChanged(user => {
    setUser(user);
    if (user)
      user.getIdTokenResult().then(res => {
        setIsAdmin(res.claims.admin === true);
      });
  }); // TODO
  auth.onIdTokenChanged(user => {
    if (user)
      user.getIdTokenResult().then(res => {
        setIsAdmin(res.claims.admin === true);
      });
  }); // TODO

  if (messagingEnabled === 0) {
    messaging
      .requestPermission()
      .then(() => {
        console.log("cloud messaging enabled");
        setMessagingEnabled(1);
        return messaging.getToken();
      })
      .then(token => {})
      .catch(err => {
        console.log(err);
        setMessagingEnabled(-1);
      });
  }

  return (
    <AuthenticationCtx.Provider
      value={{
        user: user,
        isAdmin: isAdmin,
        modalOpen: modalOpen,
        toggleModal: toggleModal,
        messagingEnabled: messagingEnabled === 1
      }}
    >
      <AuthModal toggle={toggleModal} isOpen={modalOpen} />
      {props.children}
    </AuthenticationCtx.Provider>
  );
};

export default AuthenticationCtxProvider;
