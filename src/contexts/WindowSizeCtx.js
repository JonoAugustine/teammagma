import React, { useState, createContext } from "react";
import { getWindowSize } from "../api";

export const WindowSizeCtx = createContext({ windowSize: getWindowSize() });

const WindowSizeCtxProvider = props => {
  const [windowSize, setWindowSize] = useState(getWindowSize());
  console.log(windowSize);

  window.onresize = () => setWindowSize(getWindowSize());

  return (
    <WindowSizeCtx.Provider value={{ windowSize: windowSize }}>
      {props.children}
    </WindowSizeCtx.Provider>
  );
};

export default WindowSizeCtxProvider;
