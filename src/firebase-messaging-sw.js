importScripts("https://gstatic.com/firebasejs/3.4.0/firebase-app.js");
importScripts("https://gstatic.com/firebasejs/3.4.0/firebase-messaging.js");

const firebaseConfig = {
  apiKey: "AIzaSyBCrowVat7vJ1gsOQ9ztsNKRGvc0uel3u0",
  authDomain: "teammagmastore-aae1c.firebaseapp.com",
  databaseURL: "https://teammagmastore-aae1c.firebaseio.com",
  projectId: "teammagmastore-aae1c",
  storageBucket: "teammagmastore-aae1c.appspot.com",
  messagingSenderId: "338192746726",
  appId: "1:338192746726:web:c38f7a69d6019345079327"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(payload => {
  return self.registration.showNotification(payload.message);
});
