const functions = require("firebase-functions");
const admin = require("firebase-admin");

admin.initializeApp();

const db = admin.firestore();
const moderation = db.collection("moderation");
const listings = db.collection("listings");
const offers = db.collection("offers");
const stores = db.collection("stores");
const users = db.collection("users");

const auth = admin.auth();

const messaging = admin.messaging();

const defaultResponse = {
  admin: {
    not: { status: 401, message: "missing role: admin" }
  },
  auth: {
    not: { status: 401, message: "unauthorized" }
  },
  bad: message => ({ status: 401, message: message })
};

/**
 *
 */
exports.setRoleAdmin = functions.https.onCall(async (data, ctx) => {
  if (ctx.auth.token.admin !== true) {
    return defaultResponse.admin.not;
  }

  const user = await (data.uid
    ? auth.getUser(data.uid)
    : auth.getUserByEmail(data.email));

  await auth.setCustomUserClaims(user.uid, { admin: data.admin });

  return {
    message: `(uid: ${user.uid}, email: ${user.email}) claims updated`,
    claims: user.customClaims
  };
});

exports.deny_offer = functions.https.onCall(async (data, ctx) => {
  const listings = db.collection("listings");
  const offers = db.collection("offers");

  const tListing = await listings.doc(data.lid).get();

  if (ctx.auth.uid !== tListing.data().uid)
    return { message: "Only the seller can modify a listing", status: 401 };

  const tOffer = await offers.doc(data.offer_id).get();

  await tOffer.ref.update({ denied: true });

  await tListing.ref.update({ offer_id: null });

  return { message: "Offer denied", status: 200 };
});

// Stores

exports.create_store = functions.https.onCall(async (data, ctx) => {
  if (ctx.auth.uid === null) return defaultResponse.auth.not;

  if (typeof data.name !== "string" && data.name.length <= 2) {
    return {
      status: 400,
      message: "Store must have a name longer than 2 characters"
    };
  }

  const store = await stores.where("owner.uid", "==", ctx.auth.uid).get();

  if (store.docs.length !== 0 && ctx.auth.token.admin !== true) {
    return {
      status: 409,
      message: "You cannot make more than one store."
    };
  }

  const newStore = {
    name: data.name,
    description: data.description ? data.description : "",
    owner: {
      uid: ctx.auth.uid,
      ign: ctx.auth.token.name
    },
    listings: [],
    sellers: []
  };

  const nStore = await stores.add(newStore);

  return {
    status: 200,
    message: `Store "${newStore.name}" created!`,
    store_id: nStore.id
  };
});

// Listings

exports.update_offers_name_on_listing_update = functions.firestore
  .document("listings/{lid}")
  .onUpdate(async (snap, ctx) => {
    const _offers = await offers.where("lid", "==", snap.after.id).get();
    await Promise.all(
      _offers.docs.map(d => {
        d.ref.update({
          name: snap.after.data().name,
          ask: snap.after.data().price
        });
      })
    );
  });

exports.update_offer_on_accept = functions.firestore
  .document("listings/{lid}")
  .onUpdate(async (snap, ctx) => {
    const before = snap.before.data();
    const after = snap.after.data();

    if (before.offer_id === after.offer_id) return; // ignore if offer_id doesn't change

    const auth = admin.auth();

    // If the offer_id changed from X to Y
    // Update X and notify X.buid
    if (before.offer_id !== null) {
      const before_offer_doc = offers.doc(before.offer_id);
      await before_offer_doc.update({ accepted: false, denied: false });
      const before_offer_data = await before_offer_doc.get();

      //const before_offer_user = await auth.getUser(
      //  before_offer_data.data().buid
      //);
    }

    // If the offer_id changed to a new ID
    // update new offer & notify new offer user
    if (after.offer_id !== null) {
      const new_offer_ref = offers.doc(after.offer_id);

      await new_offer_ref.update({ accepted: true, denied: false });
      const new_offer_doc = await new_offer_ref.get();

      const new_offer_user = await auth.getUser(new_offer_doc.data().buid);
    }
  });

/**
 * Remove offer from Listing on offer's deletion. Then send email
 */
exports.update_listing_on_offer_delete = functions.firestore
  .document("offers/{buid_lid}")
  .onDelete(async (snap, ctx) => {
    const lid = snap.id.split("_")[1];

    const listingDoc = await listings.doc(lid).get();

    const listing = listingDoc.data();
    if (listing.offer_id !== snap.id) return;

    listingDoc.ref.update({ offer_id: null });

    //const user = await admin.auth().getUser(listing.uid);
  });

exports.clear_offers_on_listing_delete = functions.firestore
  .document("listings/{lid}")
  .onDelete(async (snap, ctx) => {
    const targets = await offers.where("lid", "==", snap.id).get();
    await Promise.all(targets.docs.map(d => d.ref.delete()));
    const _stores = await stores
      .where("listings", "array-contains", snap.id)
      .get();
    _stores.forEach(d =>
      d.ref.update({
        listings: firebase.firestore.FieldValue.arrayRemove(snap.id)
      })
    );
  });

exports.notifySeller = functions.firestore
  .document("offers/{buid_lid}")
  .onCreate(async (snap, ctx) => {
    const offer = snap.data();
    const listing = await listings.doc(offer.lid).get();

    const seller_id = listing.data().uid;

    const seller = await users.doc(seller_id).get();

    if (seller.data().messagingToken) {
      messaging.sendToDevice(seller.data().messagingToken, {
        listing: listing,
        message: "Your listing got a new offer!"
      });
    }
  });

// User

exports.check_new_user = functions.auth.user().onCreate(async (user, ctx) => {
  const bans = await moderation.doc("bans").get();
  if (
    bans.data().igns.includes(user.displayName) ||
    bans.data().emails.includes(user.email)
  ) {
    await auth.deleteUser(user.uid);
  }
});

exports.replicate_updated_doc_ign = functions.firestore
  .document("users/{uid}")
  .onUpdate(async (snap, ctx) => {
    await auth.updateUser(snap.after.data().uid, {
      displayName: snap.after.data().ign
    });

    const _stores = await stores.where("owner.uid", "==", snap.after.id).get();
    _stores.docs.forEach(d =>
      d.ref.update({
        owner: {
          ign: snap.after.data().ign,
          uid: snap.after.data().uid
        }
      })
    );
  });

exports.remove_user_on_delete = functions.auth
  .user()
  .onDelete(async (user, ctx) => {
    await users.doc(user.uid).delete();
    // remove all offers and listings

    const removeListings = await listings.where("uid", "==", user.uid).get();
    const removeOffers = await offers.where("buid", "==", user.uid).get();

    const removeTargets = [
      ...removeListings.docs.map(d => d.ref),
      ...removeOffers.docs.map(d => d.ref)
    ];

    await Promise.all(removeTargets.map(d => d.delete()));
  });

exports.ban_user = functions.https.onCall(async (data, ctx) => {
  if (ctx.auth.token.admin !== true) return defaultResponse.admin.not;

  const target = await (data.uid
    ? auth.getUser(data.uid)
    : auth.getUserByEmail(data.email));

  await auth.deleteUser(target.uid);

  await users.doc(target.uid).delete();

  await moderation.doc("bans").update({
    emails: admin.firestore.FieldValue.arrayUnion(target.email),
    igns: admin.firestore.FieldValue.arrayUnion(target.displayName)
  });

  return { status: 200, message: "User banned and removed from database." };
});

// Store

exports.update_user_on_store_create = functions.firestore
  .document("stores/{sid}")
  .onCreate(async (snap, ctx) => {
    const uid = snap.data().owner.uid;
    const _userDoc = await users.doc(uid).get();
    await _userDoc.ref.update({ store: snap.id });
  });

exports.update_user_on_store_delete = functions.firestore
  .document("stores/{sid}")
  .onDelete(async (snap, ctx) => {
    const uid = snap.data().owner.uid;
    const _userDoc = await users.doc(uid).get();
    await _userDoc.ref.update({ store: null });
  });

exports.update_users_on_store_owner_change = functions.firestore
  .document("stores/{sid}")
  .onUpdate(async (snap, ctx) => {
    if (snap.before.data().owner.uid === snap.after.data().owner.uid) return; // ignore non-owner update
    const old_owner = await users.doc(snap.before.data().owner.uid).get();
    const new_owner = await users.doc(snap.after.data().owner.uid).get();

    await old_owner.ref.update({ store: null });

    await new_owner.ref.update({ store: snap.id });
  });

exports.transfer_store_ownership = functions.https.onCall(async (data, ctx) => {
  const _store = await stores.doc(data.store_id).get();
  if (ctx.auth.uid !== _store.data().owner.uid) return defaultResponse.auth.not;
  if (data.uid) {
    const nOwner = await users.doc(data.uid).get();
    if (nOwner.exists) {
      await _store.ref.update({
        owner: { uid: nOwner.data().uid, ign: nOwner.data().ign },
        sellers: firebase.firestore.FieldValue.arrayUnion(nOwner.data().ign)
      });
      return {
        status: 200,
        message: `Ownership Transfered to ${nOwner.data().ign} (${data.uid})`
      };
    } else {
      return defaultResponse.bad("User not found");
    }
  } else {
    return defaultResponse.bad("missing new owner user ID");
  }
});

const user_can_modify_listing_in_store = async (data, ctx) => {
  // get store doc
  const _store = await stores.doc(data.store_id).get();
  /// validate user is seller
  if (!_store.data().sellers.includes(ctx.auth.uid))
    return defaultResponse.auth.not;
  // get listing doc
  const _listing = await listings.doc(data.lid).get();
  // verify user is seller
  if (ctx.auth.uid !== _listing.data().uid) return defaultResponse.auth.not;
  // verify listing is waiting
  if (_listing.data().status.sold) {
    return defaultResponse.bad("Cannot move sold listing");
  }
  return null;
};

exports.add_listing_to_store = functions.https.onCall(async (data, ctx) => {
  const bad_resp = await user_can_modify_listing_in_store(data, ctx);
  if (bad_resp !== null) return bad_resp;

  // Add listing to store
  _store.ref.update({
    listings: firebase.firestore.FieldValue.arrayUnion(data.lid)
  });
});

exports.remove_listing_from_store = functions.https.onCall(
  async (data, ctx) => {
    const bad_resp = await user_can_modify_listing_in_store(data, ctx);
    if (bad_resp !== null) return bad_resp;

    // Add listing to store
    _store.ref.update({
      listings: firebase.firestore.FieldValue.arrayRemove(data.lid)
    });
  }
);
