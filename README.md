# [Team Magma PRO](https://teammagmapro.web.app/)

## Pokemon Revolution Online Auction House

[Go to the store now](https://teammagmapro.web.app/)

## Brief

This project aims to simplify & organize the selling, buying, & trading process
of [Pokemon Revolution Online](https://pokemonrevolution.net/) by providing
users with a clean and intuitive web app to find the items and pokemon they
need.

## Terminology

- Listing
- Offer

## About

The Team Magma store is built with [ReactJS](https://reactjs.org/) and
[Firebase](firebase.google.com/).

## Road Map

Min Viable Product

- ~~User Accounts~~
- ~~User-generated product/sale listings~~
- ~~Users can send offers for listings~~
- ~~Sellers can accept~~
- ~~Sellers can deny offers~~
- Sellers can delete listings
- Sellers can close listings
- ~~each login shoul confirm the user exists in the db.~~
- Users can see a ledger of all listings and offers complete

Post MVP Features

- User-run stores with multiple co-owners
- Trade screenshots can be added when closing a listing
- Users can request to join stores
- Listings can be made for batches of items

Styling

- ~~Listing Card in dashboard should hide image.~~
- ~~Listing card details should ne under image. make image a default open collapse~~
